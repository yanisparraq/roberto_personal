<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Home extends CI_Controller 
{
    public function __construct()
    {
        parent::__construct();        
        if(!$this->session->userdata('id'))
        {
            // $this->layout->setLayout("frontend");
            redirect(base_url()."acceso/salir");
           // redirect(base_url()."acceso/login");
        }
    }    
	//localhost/home/nosotros    
	public function index($start=null,$end=null)
    {
      //$this->layout->setLayout("ajax");
        $page = 'index';
        if ( ! file_exists(APPPATH.'views/home/'.$page.'.php'))
        {
                // Whoops, we don't have a page for that!
                show_404();
        }
   
        $data['title'] = "Radio"; // Capitalize the first letter
;
     
      
       /// var_dump($home1);
      $this->load->view('layouts/header2',compact('data'));
       $this->load->view('pages/'.$page);
      
        $this->load->view('layouts/foother'); /* */
    }
}
