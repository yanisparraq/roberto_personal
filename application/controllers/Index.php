<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Index extends CI_Controller 
{
    public function __construct()
    {
        parent::__construct();        
        if($this->session->userdata('id'))
        {
            // $this->layout->setLayout("frontend");
            redirect(base_url()."Home");
           // redirect(base_url()."acceso/login");
        }

    }    
	//localhost/home/nosotros    
  public function index()
  {
        
    $page = 'index';
    if ( ! file_exists(APPPATH.'views/pages/'.$page.'.php'))
    {
            // Whoops, we don't have a page for that!
            show_404();
    }

    $data['title'] = "Radio"; // Capitalize the first letter
    $carruseles=$this->carrusel_model->getTodos();
    $datos=$this->propiedades_model->getTodos();
    $programaciones=$this->programaciones_model->getTodos();
    //  echo $datos[1]['foto'];
    $i = 0;
    $foto = '';
  //  $home1[0][0];
    foreach($datos as $dato)
    { 
        $home1[$i][0] =$dato->foto;
        $home1[$i][1] =$dato->url;
        $home1[$i][2] =$dato->titulo;
        $i++;
    }  

    $i= 0;

    $pies=$this->pie_model->getTodos();
    foreach($pies as $pie)
    { 
        
        $home1[$i][3] =$pie->informacion;
        $home1[$i][5] =$pie->url;
     
        $i++;
    } 
    $i= 0;
    $iframes=$this->iframe_model->getTodos();
    foreach($iframes as $iframe)
    { 
        $home1[$i][4] =$iframe->contenido;
        $i++;
    } 
    $i= 0;

    $misiones=$this->mision_model->getTodos();
    foreach($misiones as $mision)
    { 
        
        $home1[$i][6] =$mision->nombre;
        $home1[$i][7] =$mision->informacion;
     
        $i++;
    }  
        $this->load->view('layouts/header',compact('home1','data'));
        $this->load->view('pages/'.$page,compact('carruseles','programaciones'));

      
        $this->load->view('layouts/foother');  
        
  }

  
  
  

  

    
}
