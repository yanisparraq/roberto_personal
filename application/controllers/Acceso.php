<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Acceso extends CI_Controller 
{
    public function __construct()
    {
        parent::__construct();        
        $this->layout->setLayout("frontendLogin");
        // redirect(base_url()."acceso/login");        
        //$this->layout->setLayout("frontendLogin");
    } 
    public function index()
    {       
        //$datos=$this->productos_model->getTodos();
        //print_r($datos);exit;
        //$this->layout->view("index",compact('datos'));
    }
    
    public function recuperar()
    {    

        if($this->input->post())
        {

             $datos=$this->usuarios_model->getContrasena($this->input->post('correo',true));
             if (!isset($datos)){
                 $this->session->set_flashdata('css','danger');
                    $this->session->set_flashdata('mensaje','El Correo no se encuentra registrado en nuestra Data');
                    redirect(base_url().'acceso/recuperar');  
             }else{
                 $this->session->set_flashdata('css','success');
                    $this->session->set_flashdata('mensaje','Hemos enviado un correo a '.$this->input->post('correo').' con su Contraseña');     
                         

             }
             $this->load->library('email');
// +2CGY2}P]EP8
            $this->email->from('soporte@ministeriosjuvenilesavocc.com', 'AVOCC');
            $this->email->to($this->input->post('correo'));
   

            $this->email->subject('Recuperación de Contraseña');
            $this->email->message('Su Contraseña es: '.$datos->clave_usuario.'');

            $this->email->send();
            redirect(base_url()); 
                        
        }   
        // $datos=$this->productos_model->getTodos();
        //print_r($datos);exit;
        $this->layout->view("recuperar");
    }
    public function membresias()
    {   
         if($this->input->post())
        {
            if($this->form_validation->run('addMembresia'))
            {
                $datos=$this->usuarios_model->getContrasena($this->input->post('correo',true));
                 if (!isset($datos))
                 {
                     $this->session->set_flashdata('css','success');
                        $this->session->set_flashdata('mensaje','Membresia Creada');  
                    $data=array
                    (
                        'nombre_de_usuario'=>$this->input->post('nombre',true)." ".$this->input->post('apellido',true),                    
                        'correo'=>$this->input->post('correo2',true),
                        'tipo_usuario_id'=>2,
                        'id_rol'=>4,
                        'clave_usuario'=>$this->input->post('password',true),
                    );
                        $insertar=$this->usuarios_model->insertar($data);   
                        

                    $this->session->set_userdata('manosenelcodigo');
                    $this->session->set_userdata('id',$insertar);
                    $this->session->set_userdata('nombre_de_usuario',$this->input->post('nombre'));
                    $this->session->set_userdata('tipo_usuario_id',1);
                    $this->session->set_userdata('id_rol',4);   
                        redirect(base_url());  
                }else{

                    $this->session->set_flashdata('css','danger');
                    $this->session->set_flashdata('mensaje','El correo <b> '.$this->input->post('correo').'</b> se encuetra registrado en nuestra data');     

                } 
             }

   

        }
        //$datos=$this->productos_model->getTodos();
        //print_r($datos);exit;
        $this->layout->view("membresias");
    }
    public function login()
    { 
        if($this->session->userdata('id'))
        {
            redirect(base_url()."home/");           
           // redirect(base_url()."acceso/login");
        }
        if($this->input->post())
        {
            if($this->form_validation->run('login'))
            {
            $datos=$this->usuarios_model->getLogin($this->input->post('correo',true),$this->input->post('pass',true));
            if (!isset($datos)){
                 $this->session->set_flashdata('css','danger');
                    $this->session->set_flashdata('mensaje','Error de Email ó Contraseña');

            }else
            {
                
                $this->session->set_userdata('ministerioJuvenil');
                $this->session->set_userdata('id',$datos->id);

                $this->session->set_userdata('id_rol',$datos->id_rol);
              
              
              
                redirect(base_url()."Home");
            }  }     


        }
         $this->layout->view('login');
        
    }
    public function restringido1()
    {
        if($this->session->userdata('id'))
        {
            $this->layout->view("restringido1");
        }else
        {
            redirect(base_url()."acceso/login");
        }
    }
    public function salir()
        {
            $this->session->sess_destroy("manosenelcodigo");
            redirect(base_url().'acceso/login',  301);
        }
    

    public function add()
    {
        //redirect(base_url()."productos");
        if($this->input->post())
        {
            
                //  redirect(base_url()."productos");
                $data=array
                (
                    'nombre_de_usuario'=>$this->input->post('nombre_de_usuario',true).' '.$this->input->post('apellido',true),
                    
                    'correo'=>$this->input->post('correo',true),
                     'telefono'=>$this->input->post('telefono',true),
                    'tipo_usuario_id'=>1,
                    'id_rol'=>2,
                    'clave_usuario'=>$this->input->post('password',true),
                    'id_club'=>$this->input->post('id_club',true),
                    'id_categoria'=>7,
                    
                );
                    $insertar1=$this->usuarios_model->insertar($data);                              
                    $this->session->set_flashdata('css','success');
                    $this->session->set_flashdata('mensaje','El registro se ha creado exitosamente');
                    redirect(base_url()."acceso/login");            
             }
        
  //  $tipo_usuario=$this->tipo_usuario_model->getTodos();
     // $categorias=$this->categorias_model->getTodos();
       
        $zonas=$this->zonas_model->getTodos();
        $this->layout->view("add",compact('zonas'));
    }



    public function add2()
    {
        //redirect(base_url()."productos");
        if($this->input->post())
        {
            
                //  redirect(base_url()."productos");
                $data=array
                (
                    'nombre_de_usuario'=>$this->input->post('nombre_de_usuario',true).' '.$this->input->post('apellido',true),
                    
                    'correo'=>$this->input->post('correo',true),
                     'telefono'=>$this->input->post('telefono',true),
                    'tipo_usuario_id'=>1,
                    'id_rol'=>3,
                    'clave_usuario'=>$this->input->post('password',true),
                    'id_club'=>7,
                    'id_zona'=>$this->input->post('id_zona',true),
                    
                );
                    $insertar1=$this->usuarios_model->insertar($data);                              
                    $this->session->set_flashdata('css','success');
                    $this->session->set_flashdata('mensaje','El registro se ha creado exitosamente');
                    redirect(base_url()."acceso/login");            
             }
        
  //  $tipo_usuario=$this->tipo_usuario_model->getTodos();
     // $categorias=$this->categorias_model->getTodos();
       
        $zonas=$this->zonas_model->getTodos();
        $this->layout->view("add2",compact('zonas'));
    }
}