<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Unidades extends CI_Controller 
{
    public function __construct()
    {
        parent::__construct();        
       
    }    
    public function index()
    {      
        
        if($this->uri->segment(3))
        {
            $pagina=$this->uri->segment(3);
        }else
        {
            $pagina=0;
        }
        $porpagina=10;
        //zona de car;ga de los datos
        $datos=$this->unidades_model->getTodosPaginacion2($pagina,$porpagina,"limit");
        
        $cuantos=$this->unidades_model->getTodosPaginacion2($pagina,$porpagina,"cuantos");           //zona de configuración de la librería pagination
        $config['base_url']=base_url()."unidades/index";
        $config['total_rows']=$cuantos;
        $config['per_page']=$porpagina;
        $config['uri_segment']='3';
        $config['num_links']='4';
        $config['first_link']='Primero';
        $config['next_link']='Siguiente';
        $config['prev_link']='Anterior';
        $config['last_link']='Última';
        
        $config['full_tag_open']='<ul class="page-numbers">';
        
       
        $config['first_tag_open'] = '<li class="page-item">';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li class="page-item">';
        $config['last_tag_close'] = '</li>';
        $config['next_tag_open'] = '<li class="page-item">';
        $config['next_tag_close'] = '</li>';
        $config['prev_tag_open'] = '<li class="page-item">';
        $config['prev_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="page-item active"><a class="page-link"><b>';
        $config['cur_tag_close'] = '</b></a></li>';
        $config['num_tag_open'] = '<li class="page-item">';
        $config['num_tag_close'] = '</li>';    
        
        
        $config['full_tag_close']='</ul>';


        $this->pagination->initialize($config);

        $page = 'index';
        if ( ! file_exists(APPPATH.'views/unidades/'.$page.'.php'))
        {
                // Whoops, we don't have a page for that!
                show_404();
        }
    
        $data['title'] = "Unidades"; // Capitalize the first letter
         

      $this->load->view('layouts/header2',compact('data'));
      $this->load->view('unidades/'.$page, compact('datos','cuantos','pagina')); 
      $this->load->view('layouts/foother'); 

        //$this->layout->view("index",compact('datos','cuantos','pagina'));
        
    } 

    
  
    public function edit($id=null,$pagina=null)
    {


        
            $page = "edit";            
            // redirect(base_url()."usuarios");       
            // redirect(base_url()."usuarios");
            $dato=$this->unidades_model->getid($id);
            if($this->input->post())
            {
                //primero subimos el archivo
                if($this->form_validation->run('unidades'))
                {
                   

                        $data=array
						(

                            'nombre'=>$this->input->post('nombre',true), 
							'abreviatura'=>$this->input->post('abreviatura',true),  
																							
						);

                   


                    $this->unidades_model->update($data,$id);
                    $this->session->set_flashdata('css','success');
                    $this->session->set_flashdata('mensaje','El registro se ha modificado exitosamente');
                    redirect(base_url()."unidades"); 

                }
                
              
           
                
            }
         
            $data['title'] = "Editar Unidad"; // Capitalize the first letter
       
          
         $this->load->view('layouts/header2',compact('data','dato','id'));
          $this->load->view('unidades/'.$page); 
          $this->load->view('layouts/foother'); 
                // $this->layout->view("edit",compact('dato','id'));
                //  $this->layout->view('add',compact('datos','roles','tipo_usuario','id','pagina'));
    }




    
   




    public function add()
    {
            $page = "add";  
            if($this->input->post())
            {
                //primero subimos el archivo
                if($this->form_validation->run('unidades'))
              
                {
                  

                        $data=array
						(
                            
                                'nombre'=>$this->input->post('nombre',true),
                                'abreviatura'=>$this->input->post('abreviatura',true),
                               
                              
																							
						);

                    
                    $this->unidades_model->insertar($data);
                    $this->session->set_flashdata('css','success');
                    $this->session->set_flashdata('mensaje','El registro se ha modificado exitosamente');
                    redirect(base_url()."unidades"); 
                }
            }         
            $data['title'] = "Unidades"; // Capitalize the first letter       

          
            $this->load->view('layouts/header2',compact('data'));
            $this->load->view('unidades/'.$page);
            $this->load->view('layouts/foother', $data);  
                // $this->layout->view("edit",compact('dato','id'));
                //  $this->layout->view('add',compact('datos','roles','tipo_usuario','id','pagina'));
    }
  


    public function delete($id=null)
    {
        
        if(!$id){show_404();}  
        $this->unidades_model->delete($id);
        $this->session->set_flashdata('css','success');
        $this->session->set_flashdata('mensaje','El registro se ha eliminado exitosamente');
        redirect(base_url()."unidades");
    }






}




