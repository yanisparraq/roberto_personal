<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Configuraciones extends CI_Controller 
{
    public function __construct()
    {
        parent::__construct();        
       
    }    
    public function index()
    {      
        
        if($this->uri->segment(3))
        {
            $pagina=$this->uri->segment(3);
        }else
        {
            $pagina=0;
        }
        $porpagina=10;
        //zona de car;ga de los datos
        $datos=$this->propiedades_model->getTodosPaginacion2($pagina,$porpagina,"limit");
        
        $cuantos=$this->propiedades_model->getTodosPaginacion2($pagina,$porpagina,"cuantos");           //zona de configuración de la librería pagination
        $config['base_url']=base_url()."propiedades/index";
        $config['total_rows']=$cuantos;
        $config['per_page']=$porpagina;
        $config['uri_segment']='3';
        $config['num_links']='4';
        $config['first_link']='Primero';
        $config['next_link']='Siguiente';
        $config['prev_link']='Anterior';
        $config['last_link']='Última';
        
        $config['full_tag_open']='<ul class="page-numbers">';
        
       
        $config['first_tag_open'] = '<li class="page-item">';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li class="page-item">';
        $config['last_tag_close'] = '</li>';
        $config['next_tag_open'] = '<li class="page-item">';
        $config['next_tag_close'] = '</li>';
        $config['prev_tag_open'] = '<li class="page-item">';
        $config['prev_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="page-item active"><a class="page-link"><b>';
        $config['cur_tag_close'] = '</b></a></li>';
        $config['num_tag_open'] = '<li class="page-item">';
        $config['num_tag_close'] = '</li>';    
        
        
        $config['full_tag_close']='</ul>';


        $this->pagination->initialize($config);

        $page = 'index';
        if ( ! file_exists(APPPATH.'views/configuraciones/'.$page.'.php'))
        {
                // Whoops, we don't have a page for that!
                show_404();
        }
    
        $data['title'] = "Home"; // Capitalize the first letter
          $datos2=$this->propiedades_model->getTodos();
        //  echo $datos[1]['foto'];
       
      $this->load->view('layouts/header2',compact('data'));
      $this->load->view('configuraciones/'.$page, compact('datos','cuantos','pagina')); 
      $this->load->view('layouts/foother'); 

        //$this->layout->view("index",compact('datos','cuantos','pagina'));
        
    } 

    
  
    public function edit($id=null,$pagina=null)
    {


        
            $page = "edit";            
            // redirect(base_url()."usuarios");       
            // redirect(base_url()."usuarios");
            $dato=$this->propiedades_model->getid($id);
            if($this->input->post())
            {
                //primero subimos el archivo
                if($this->form_validation->run('carrusel'))
                {
                    if($_FILES['file']['type']) 
                    {
                        $ruta = "public/img";
                        switch($_FILES['file']['type'])
                        {
                                case 'image/jpeg':
                                        //insertamos el registro con la foto vacía
                                        //subimos la foto
                                        $picture= $id.".jpg";
                                        copy($_FILES['file']['tmp_name'],$ruta."/".$picture);
                                        //actualizamos el registro con el nombre de la foto
                                break;
                                case 'image/png':
                                        //insertamos el registro con la foto vacía
                                        //subimos la foto
                            $picture=$id.".png";
                            copy($_FILES['file']['tmp_name'],$ruta."/".$picture);  
                                break;
                                default:
                                        $this->session->set_flashdata('css','danger');
                                        $this->session->set_flashdata('mensaje','Sólo se aceptan fotos en formato JPG o PNG');
                                //   $this->layout->view('fotos',compact('datos','id','pagina'));
                                        //redirect(base_url()."productos/fotos/".$this->input->post('id',true)."/".$this->input->post('pagina',true));
                                        redirect(base_url()."configuraciones/edit/".$id);  
                                        break;
                        }
                        $data=array
						(
                            'titulo'=>$this->input->post('titulo',true),                             
                            'url'=>$this->input->post('url',true), 
							'foto'=>$ruta."/".$picture, 																
						);
                       
                    }else{

                        $data=array
						(

                            'titulo'=>$this->input->post('titulo',true), 
							'url'=>$this->input->post('url',true),  
																							
						);

                    }


                    $this->propiedades_model->update($data,$id);
                    $this->session->set_flashdata('css','success');
                    $this->session->set_flashdata('mensaje','El registro se ha modificado exitosamente');
                    redirect(base_url()."configuraciones"); 

                }
                
              
           
                
            }
         
            $data['title'] = "Home"; // Capitalize the first letter
       
            $datos2=$this->propiedades_model->getTodos();
            //  echo $datos[1]['foto'];
            $i = 0;
            $foto = '';
          //  $home1[0][0];
          foreach($datos2 as $dato2)
          { 
              $home1[$i][0] =$dato2->foto;
              $home1[$i][1] =$dato2->url;
              $home1[$i][2] =$dato2->titulo;
              $i++;
          }  
    
          $i= 0;
    
          $pies=$this->pie_model->getTodos();
          foreach($pies as $pie)
          { 
              
              $home1[$i][3] =$pie->informacion;$home1[$i][5] =$pie->url;
           
       
            
              $i++;
          } 
        
         $this->load->view('layouts/header2',compact('home1','data','dato','id'));
          $this->load->view('configuraciones/'.$page); 
          $this->load->view('layouts/foother'); 
                // $this->layout->view("edit",compact('dato','id'));
                //  $this->layout->view('add',compact('datos','roles','tipo_usuario','id','pagina'));
    }




    public function logo($id=null,$pagina=null)
    {


        
           
            if($this->input->post())
            {
                //primero subimos el archivo

              

                    if($_FILES['file']['type']) 
                    {


                        $ruta = "public/img";
                        switch($_FILES['file']['type'])
                        {
                                case 'image/jpeg':
                                        //insertamos el registro con la foto vacía
                                        //subimos la foto
                                        $picture= "logo.jpg";
                                        copy($_FILES['file']['tmp_name'],$ruta."/".$picture);
                                        //actualizamos el registro con el nombre de la foto
                                break;
                                case 'image/png':
                                        //insertamos el registro con la foto vacía
                                        //subimos la foto
                            $picture="logo.png";
                            copy($_FILES['file']['tmp_name'],$ruta."/".$picture);  
                                break;
                                default:
                                        $this->session->set_flashdata('css','danger');
                                        $this->session->set_flashdata('mensaje','Sólo se aceptan fotos en formato JPG o PNG');
                                //   $this->layout->view('fotos',compact('datos','id','pagina'));
                                        //redirect(base_url()."productos/fotos/".$this->input->post('id',true)."/".$this->input->post('pagina',true));
                                        redirect(base_url()."Home");  
                                        break;
                        }

                        $data=array
						(
								'url'=>$this->input->post('url',true),  
								'foto'=>$ruta."/".$picture, 																
						);
                       
                    }else{

                        $data=array
						(
								'url'=>$this->input->post('url',true),  
																							
						);

                    }


                    $this->propiedades_model->update($data,47);
                    $this->session->set_flashdata('css','success');
                    $this->session->set_flashdata('mensaje','El registro se ha modificado exitosamente');
                    redirect(base_url()."Home"); 

               
                
              
           
                
            }
           
    }
   


  
}




