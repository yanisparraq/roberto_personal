<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pie extends CI_Controller 
{
    public function __construct()
    {
        parent::__construct(); 
    }    
    public function index()
    {      
        
        if($this->uri->segment(3))
        {
            $pagina=$this->uri->segment(3);
        }else
        {
            $pagina=0;
        }
        $porpagina=10;
        //zona de car;ga de los datos
        $datos=$this->pie_model->getTodosPaginacion2($pagina,$porpagina,"limit");
        
        $cuantos=$this->pie_model->getTodosPaginacion2($pagina,$porpagina,"cuantos");           //zona de configuración de la librería pagination
        $config['base_url']=base_url()."pie/index";
        $config['total_rows']=$cuantos;
        $config['per_page']=$porpagina;
        $config['uri_segment']='3';
        $config['num_links']='4';
        $config['first_link']='Primero';
        $config['next_link']='Siguiente';
        $config['prev_link']='Anterior';
        $config['last_link']='Última';
        
        $config['full_tag_open']='<ul class="page-numbers">';
        
       
        $config['first_tag_open'] = '<li class="page-item">';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li class="page-item">';
        $config['last_tag_close'] = '</li>';
        $config['next_tag_open'] = '<li class="page-item">';
        $config['next_tag_close'] = '</li>';
        $config['prev_tag_open'] = '<li class="page-item">';
        $config['prev_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="page-item active"><a class="page-link"><b>';
        $config['cur_tag_close'] = '</b></a></li>';
        $config['num_tag_open'] = '<li class="page-item">';
        $config['num_tag_close'] = '</li>';    
        
        
        $config['full_tag_close']='</ul>';


        $this->pagination->initialize($config);

        $page = 'index';

    
        $data['title'] = "Home"; // Capitalize the first letter
          $datos2=$this->propiedades_model->getTodos();
        //  echo $datos[1]['foto'];
        $i = 0;
        $foto = '';
      //  $home1[0][0];
      foreach($datos2 as $dato)
      { 
          $home1[$i][0] =$dato->foto;
          $home1[$i][1] =$dato->url;
          $home1[$i][2] =$dato->titulo;
          $i++;
      }  

      $i= 0;

      $pies=$this->pie_model->getTodos();
      foreach($pies as $pie)
      { 
          
          $home1[$i][3] =$pie->informacion;$home1[$i][5] =$pie->url;
       
   
        
          $i++;
      } 
      $this->load->view('layouts/header2',compact('home1','data'));
      $this->load->view('pie/'.$page, compact('datos','cuantos','pagina'));

    
      $this->load->view('layouts/foother'); 


        //$this->layout->view("index",compact('datos','cuantos','pagina'));
        
    } 

    
  
    public function edit($id=null,$pagina=null)
    {
        $page = "edit";            
            // redirect(base_url()."usuarios");       
            // redirect(base_url()."usuarios");
            $dato=$this->pie_model->getid($id);
            if($this->input->post())
            {
                //primero subimos el archivo
                if($this->form_validation->run('pie'))
                {
                    

                        $data=array
						(
                           
                            'informacion'=>$this->input->post('informacion',true), 
                            'url'=>$this->input->post('url',true),  
																							
						);

                    


                    $this->pie_model->update($data,$id);
                    $this->session->set_flashdata('css','success');
                    $this->session->set_flashdata('mensaje','El registro se ha modificado exitosamente');
                    redirect(base_url()."pie"); 

                }
                
              
           
                
            }
         
            $data['title'] = "Pie de Pagina"; // Capitalize the first letter
       
            $datos2=$this->propiedades_model->getTodos();
            //  echo $datos[1]['foto'];
            $i = 0;
            $foto = '';
          //  $home1[0][0];
          foreach($datos2 as $dato2)
          { 
              $home1[$i][0] =$dato2->foto;
              $home1[$i][1] =$dato2->url;
              $home1[$i][2] =$dato2->titulo;
              $i++;
          }  
    
          $i= 0;
    
          $pies=$this->pie_model->getTodos();
          foreach($pies as $pie)
          { 
              
              $home1[$i][3] =$pie->informacion;$home1[$i][5] =$pie->url;
           
       
            
              $i++;
          } 



            $this->load->view('layouts/header2',compact('home1','data'));
            $this->load->view('pie/'.$page, compact('dato','id'));
            $this->load->view('layouts/foother', $data);  
                // $this->layout->view("edit",compact('dato','id'));
                //  $this->layout->view('add',compact('datos','roles','tipo_usuario','id','pagina'));
    }




   









}




