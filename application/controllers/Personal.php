<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Personal extends CI_Controller 
{
    public function __construct()
    {
        parent::__construct();        
       
    }    
    public function index()
    {      
        
        if($this->uri->segment(3))
        {
            $pagina=$this->uri->segment(3);
        }else
        {
            $pagina=0;
        }
        $porpagina=10;
        //zona de car;ga de los datos
        $datos=$this->personal_model->getTodosPaginacion2($pagina,$porpagina,"limit");
        
        $cuantos=$this->personal_model->getTodosPaginacion2($pagina,$porpagina,"cuantos");           //zona de configuración de la librería pagination
        $config['base_url']=base_url()."personal/index";
        $config['total_rows']=$cuantos;
        $config['per_page']=$porpagina;
        $config['uri_segment']='3';
        $config['num_links']='4';
        $config['first_link']='Primero';
        $config['next_link']='Siguiente';
        $config['prev_link']='Anterior';
        $config['last_link']='Última';
        
        $config['full_tag_open']='<ul class="page-numbers">';
        
       
        $config['first_tag_open'] = '<li class="page-item">';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li class="page-item">';
        $config['last_tag_close'] = '</li>';
        $config['next_tag_open'] = '<li class="page-item">';
        $config['next_tag_close'] = '</li>';
        $config['prev_tag_open'] = '<li class="page-item">';
        $config['prev_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="page-item active"><a class="page-link"><b>';
        $config['cur_tag_close'] = '</b></a></li>';
        $config['num_tag_open'] = '<li class="page-item">';
        $config['num_tag_close'] = '</li>';    
        
        
        $config['full_tag_close']='</ul>';


        $this->pagination->initialize($config);

        $page = 'index';
        if ( ! file_exists(APPPATH.'views/personal/'.$page.'.php'))
        {
                // Whoops, we don't have a page for that!
                show_404();
        }
    
        $data['title'] = "personal"; // Capitalize the first letter
         
       
      $this->load->view('layouts/header2',compact('data'));
      $this->load->view('personal/'.$page, compact('datos','cuantos','pagina')); 
      $this->load->view('layouts/foother'); 

        //$this->layout->view("index",compact('datos','cuantos','pagina'));
        
    } 

    
  
    public function edit($id=null,$pagina=null)
    {


        
            $page = "edit";            
            // redirect(base_url()."usuarios");       
            // redirect(base_url()."usuarios");
            $dato=$this->personal_model->getid($id);
            if($this->input->post())
            {
                //primero subimos el archivo
                if($this->form_validation->run('personal'))
                {
                   

                        $data=array
						(

                            'nombre'=>$this->input->post('nombre',true), 
							'abreviatura'=>$this->input->post('abreviatura',true),  
																							
						);

                   


                    $this->personal_model->update($data,$id);
                    $this->session->set_flashdata('css','success');
                    $this->session->set_flashdata('mensaje','El registro se ha modificado exitosamente');
                    redirect(base_url()."personal"); 

                }
                
              
           
                
            }
         
            $data['title'] = "Editar Unidad"; // Capitalize the first letter
       
          
         $this->load->view('layouts/header2',compact('data','dato','id'));
          $this->load->view('personal/'.$page); 
          $this->load->view('layouts/foother'); 
                // $this->layout->view("edit",compact('dato','id'));
                //  $this->layout->view('add',compact('datos','roles','tipo_usuario','id','pagina'));
    }




    
   




    public function add()
    {
            $page = "add";  
            if($this->input->post())
            {

                var_dump($this->input->post());
              
               
                  
/*
                        $data=array
						(
                            
                            'nombre'=>$this->input->post('nombre',true),

                            'nombre2'=>$this->input->post('nombre2',true),
                            'apellido'=>$this->input->post('apellido',true),
                            'apellido2'=>$this->input->post('apellido2',true),
                            'dni'=>$this->input->post('dni',true),
                            'cip'=>$this->input->post('cip',true),
                            'estado_civil'=>$this->input->post('estado_civil',true),
                            'fecha_nacimiento'=>$this->input->post('fecha_nacimiento',true),
                            'hijo'=>$this->input->post('hijo',true),
                            'lugar_nacimiento'=>$this->input->post('lugar_nacimiento',true),
                            'telefono'=>$this->input->post('telefono',true),
                            'telefono2'=>$this->input->post('telefono2',true),
                            'dirección'=>$this->input->post('dirección',true),
                            'id_grado'=>$this->input->post('id_grado',true),
                            'id_unidad'=>$this->input->post('id_unidad',true),
                            'id_grupo'=>$this->input->post('id_grupo',true),
                            'id_especialidad'=>$this->input->post('id_especialidad',true),
                            'condicion'=>$this->input->post('condicion',true),
                            'puesto'=>$this->input->post('puesto',true),
                            'fecha_contrato'=>$this->input->post('fecha_contrato',true),
                            'fecha_nombrado'=>$this->input->post('fecha_nombrado',true),
                            'lugarTrabajo1'=>$this->input->post('lugarTrabajo1',true),
                            'lugarTrabaajofecha1'=>$this->input->post('lugarTrabaajofecha1',true),
                            'lugarTrababajo2'=>$this->input->post('lugarTrababajo2',true),
                            'lugarTrababajofecha2'=>$this->input->post('lugarTrababajofecha2',true),
                            'lugarTrabajo3'=>$this->input->post('lugarTrabajo3',true),
                            'lugarTrabajajofecha3'=>$this->input->post('lugarTrabajajofecha3',true),
                            'sanciones'=>$this->input->post('sanciones',true),
                            'medicos'=>$this->input->post('medicos',true),
                                
                               
                              
																							
						);*/

                    
                    $this->personal_model->insertar($this->input->post());
                    $this->session->set_flashdata('css','success');
                    $this->session->set_flashdata('mensaje','El registro se ha modificado exitosamente');
                    redirect(base_url()."personal"); 
               


                
            }   else{

                
            $data['title'] = "personal"; // Capitalize the first letter       

            $grados = $this->grados_model->getTodos();
            $unidades = $this->unidades_model->getTodos();
            $grupos = $this->grupos_model->getTodos();
            $especialidades = $this->especialidades_model->getTodos();
            $this->load->view('layouts/header2',compact('data','grados','unidades','grupos','especialidades'));
            $this->load->view('personal/'.$page);
            $this->load->view('layouts/foother', $data);  



        } 
                // $this->layout->view("edit",compact('dato','id'));
                //  $this->layout->view('add',compact('datos','roles','tipo_usuario','id','pagina'));
    }
  


    public function delete($id=null)
    {
        
        if(!$id){show_404();}  
        $this->personal_model->delete($id);
        $this->session->set_flashdata('css','success');
        $this->session->set_flashdata('mensaje','El registro se ha eliminado exitosamente');
        redirect(base_url()."personal");
    }




    public function pdf($id=null)
    {
        
        if(!$id){show_404();}  
        $personal = $this->personal_model->getid($id);
        
        $this->load->view('personal/pdf',compact('personal'));

    }






}




