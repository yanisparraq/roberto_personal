<div class="content-wrapper-before gradient-45deg-indigo-purple"></div>
<div class="breadcrumbs-dark pb-0 pt-4" id="breadcrumbs-wrapper">
  <!-- Search for small screen-->
  <div class="container">
    <div class="row">
      <div class="col s10 m6 l6">
        <h5 class="breadcrumbs-title mt-0 mb-0"><span>Agregar Grupos Ocupacionales</span></h5>
        <ol class="breadcrumbs mb-0">
          <li class="breadcrumb-item"><a href="<?php echo base_url()?>">Home</a>


          <li class="breadcrumb-item">
              <a href="<?php echo base_url()?>grupos">
              Grupos Ocupacionales
            </a>
          </li>
            <li class="breadcrumb-item active">Agregar
            </li>
        </ol>
      </div>
      
      
    </div>
  </div>
</div>


<div class="row">
  <div class="col s12">
    <div id="input-fields" class="card card-tabs">
      <div class="card-content">
        <div class="card-title">
          <div class="row">
            <div class="col s12 m6 l10">
              <h4 class="card-title">Complete Los  Campos 


             
              </h4>
            </div>            
          </div>
        </div>
        <div id="view-input-fields">
          <div class="row">


            <?php
            $errors=validation_errors('<li>','</li>');
              if($errors!="")
              {
                  ?>

              <div class="card-alert card red lighten-5">
                <div class="card-content red-text">
                      <ul>
                          <?php echo $errors;?>
                      </ul>
                </div>
               
              </div>
                 
                  <?php
              } ?> 
              <form action="<?php echo base_url();?>grupos/add" id="addUsuarios" name = "addUsuarios" method="post">
            
              <div class="row">
                <div class="input-field col s4">
                  <i class="material-icons prefix">streetview</i>
                  <input id="icon_prefix" type="text" class="validate"    name="nombre" value="<?php echo set_value_input(array(),'nombre_campo','nombre_campo')?>" >
                  <label for="icon_prefix">Nombre del Grupo Ocupacional</label>
                </div>
                <div class="input-field col s4">
                  <i class="material-icons prefix">unfold_less</i>
                  <input id="icon_telephone" type="text"  name="abreviatura"  value="<?php echo set_value_input(array(),'abreviatura','abreviatura')?>" >
                  <label for="icon_telephone">Abreviatura</label>
                </div>

                

                <div class="input-field col s12">
                  <button class="btn waves-effect waves-light right" type="submit" name="action">Enviar
                    <i class="material-icons right">send</i>
                  </button>
                </div>
              </div>
            </form>
         
          </div>
        </div>
      </div>
    </div>
  </div>
</div>









 <!-- BEGIN: Header-->
   

    <!-- END: Footer-->
    <!-- BEGIN VENDOR JS-->
    <script src="<?php echo base_url();?>public/app-assets/js/vendors.min.js"></script>
    <!-- BEGIN VENDOR JS-->
    <!-- BEGIN PAGE VENDOR JS-->
    <!-- END PAGE VENDOR JS-->
    <!-- BEGIN THEME  JS-->
    <script src="<?php echo base_url();?>public/app-assets/js/plugins.js"></script>

    <!-- END THEME  JS-->
    <!-- BEGIN PAGE LEVEL JS-->
  
    <!-- END PAGE LEVEL JS-->
 