


  <div class="content-wrapper-before gradient-45deg-indigo-purple"></div>
<div class="breadcrumbs-dark pb-0 pt-4" id="breadcrumbs-wrapper">
  <!-- Search for small screen-->
  <div class="container">
    <div class="row">
      <div class="col s10 m6 l6">
        <h5 class="breadcrumbs-title mt-0 mb-0"><span>Personal</span></h5>
        <ol class="breadcrumbs mb-0">
          <li class="breadcrumb-item"><a href="<?php echo base_url()?>Home">Inicio</a>

          <li class="breadcrumb-item active">Personal
          </li>
        </ol>
      </div>

      
      <div class="col s2 m6 l6">
        <a class="btn dropdown-settings waves-effect waves-light breadcrumbs-btn right" 
          href="<?php echo base_url()?>personal/add">
          <i class='material-icons left'>add</i><span class="hide-on-small-onl">Agregar</span>
        </a>        
      </div>
      
    </div>
  </div>
</div>



<div class="col s12">
  <div class="container">
    <div class="section section-data-tables">
     

      <div class="row">
        <div class="col s12 m12 l12">
          <div id="button-trigger" class="card card card-default scrollspy">
            <div class="card-content">
              
              <div class="row">
               
                <div class="col s12">
                  <table id="data-table-simple" class="bordered striped Highlight">
                    <thead>
                      <tr>
                        <th>Nombre</th>
                        <th>DNI</th>
                        <th>Puesto</th>
                                             
                        <th> 
                          <div class="row">
                          <div class="col s12">Acción</div>
                           </div>
                        </th>
                       
                        
                      </tr>
                    </thead>
                    <tbody>
                    <?php
                      foreach($datos as $dato)
                      {                  
                    ?>
                      <tr>
                      <td><?php echo $dato->nombre.' '.$dato->apellido?></td>
                      <td><?php echo $dato->dni?></td>
                      <td><?php echo $dato->puesto?></td>
                        
                       
                 
                      <td  class="text-center" >
                        <div class="row">
                        <div class="col s6"><a href="<?php echo base_url()?>personal/pdf/<?php echo $dato->id?>" ><i class="material-icons m-10">picture_as_pdf</i></a></div>
                      
                          <div class="col s6"><a  href="<?php echo base_url()?>personal/edit/<?php echo $dato->id?>" ><i class="material-icons">edit</i></a></div>
                          <div class="col s6"><a href="<?php echo base_url()?>personal/delete/<?php echo $dato->id?>" ><i class="material-icons m-10">delete</i></a></div>
                        </div>


                        
                        </td>

                        
                        </td>
                       
                      </tr>
                      <?php
                      }                  
                    ?>
                    </tbody>
                   
                  </table>


<?php
 echo $this->pagination->create_links()?> 
                </div>
 
  
              </div>
           
            </div>
           
          </div>
         
        </div>
      </div>
    
    </div>
  
  </div>

</div>
<?php
    if($this->session->flashdata('mensaje')!='')
    {
       ?>
      
          <script> 
      // alertas('<?php echo $this->session->flashdata('css')?>','<?php echo $this->session->flashdata('mensaje')?>');
      swal({
    title: "<?php echo $this->session->flashdata('mensaje')?> ",
    text: " ",
    icon: '<?php echo $this->session->flashdata('css')?>',
    
    timer: 2000,
    buttons: false
  }); </script>
          

      
       <?php 
    }
    ?> 








    






