<?php

require_once APPPATH.'third_party/fpdf/fpdf.php';

class PDF extends FPDF
{
  protected $B = 0;
  protected $I = 0;
  protected $U = 0;
  protected $HREF = '';





// Cabecera de página
/*
function Header()
{
    // Logo
  //  $this->Image('logo_pb.png',10,8,33);
    // Arial bold 15
    $this->SetFont('Arial','B',15);
    // Movernos a la derecha
    $this->Cell(80);
    // Título
    $this->Cell(30,10,'Title',1,0,'C');
    // Salto de línea
    $this->Ln(20);
}
*/
// Pie de página
  function Footer()
  {
      // Posición: a 1,5 cm del final
      $this->SetY(-15);
      // Arial italic 8
      $this->SetFont('Arial','I',8);
      // Número de página
      $this->Cell(0,10,utf8_decode('Página ').$this->PageNo().'/{nb}',0,0,'C');
  }
}

require_once APPPATH.'third_party/phpqrcode/qrlib.php';


$text_qr = base_url().'personal/pdf/'.$personal->id;
$ruta_qr = "public/Qr/".$personal->nombre."_".$personal->apellido."".$personal->id.".png";

QRcode::png($text_qr, $ruta_qr, 'Q',15, 0);



// Creación del objeto de la clase heredada
$pdf = new PDF('P','mm','A4');
$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->SetLeftMargin(20);
$pdf->SetFont('Times','B',12);
$fill = false;

$pdf->Image($ruta_qr,140,240,33);


$pdf->Cell(1);

$pdf->Cell(201,8,utf8_decode('1. DATOS PERSONALES'),'',1,'',false);



$pdf->SetDrawColor(0,0,0);
$pdf->SetLineWidth(0);
$pdf->SetTextColor(0);
// Título    $pdf->Cell(90,4,'______________________','',0,'C',false);
$pdf->Ln(10);
$pdf->SetFillColor(0,0,0);
$pdf->SetFont('Times','B',10);
$pdf->SetFillColor(500);
$pdf->Cell(45,8,utf8_decode('GRADO: ').''.utf8_decode($personal->nombre_grado),'LRTB',0,'',true);






$pdf->Cell(90,8,utf8_decode('GRUPO OCUPACIONAL: ').''.utf8_decode($personal->nombre_ocupacional),'LRTB',0,'',true);


$pdf->Cell(45,8,utf8_decode('CONDICIÓN: ').''.utf8_decode($personal->condicion),'LRTB',1,'',true);







$pdf->Cell(100,8,utf8_decode('APELLIDOS Y NOMBRES: ').''.utf8_decode($personal->nombre.' '.$personal->nombre2.' '.$personal->apellido.' '.$personal->apellido2),'LRTB',0,'',true);


$pdf->Cell(80,8,utf8_decode('ESPECIALIDAD: ').''.utf8_decode($personal->nombre_especialidad),'LRTB',1,'',true);







$pdf->Cell(100,8,utf8_decode('UNIDAD DE ORIGEN: ').''.utf8_decode($personal->nombre_unidad),'LRTB',0,'',true);





$pdf->Cell(80,8,utf8_decode('PUESTO: ').''.utf8_decode($personal->puesto),'LRTB',1,'',true);




$pdf->Cell(130,8,utf8_decode('ESTADO CIVIL: ').''.utf8_decode($personal->estado_civil),'LRTB',0,'',true);




$pdf->Cell(50,8,utf8_decode('TIEMPO DE SERVICIO: ').''.calculaEdad($personal->fecha_contrato),'LRTB',1,'',true);











////estado civil

$pdf->Cell(90,8,utf8_decode('CIP: ').''.utf8_decode($personal->cip),'LRTB',0,'',true);






$pdf->Cell(90,8,utf8_decode('DNI: ').''.utf8_decode($personal->dni),'LRTB',1,'',true);


////// 6


$pdf->MultiCell(180, 8,utf8_decode('LUGAR NACIMIENTO :').''.utf8_decode($personal->lugar_nacimiento),1,'J','false' );




////// 6

$pdf->Cell(180,8,utf8_decode('FECHA NACIMIENTO: ').''.fecha($personal->fecha_nacimiento),'LRTB',1,'',true);


////// 6


$pdf->Cell(180,8,utf8_decode('FECHA INGRESO CONTRATADO: ').''.fecha($personal->fecha_contrato),'LRTB',1,'',true);




$pdf->Cell(180,8,utf8_decode('FECHA INGRESO NOMBRADO: ').''.fecha($personal->fecha_nombrado),'LRTB',1,'',true);


////// 6





$pdf->Cell(180,8,utf8_decode('FECHA ULTIMO ASCENSO: ').''.fecha($personal->fecha_ascenso),'LRTB',1,'',true);


////// 6

$pdf->Cell(180,8,utf8_decode('TRES ULTIMOS LUGARES DE TRABAJO Y FECHA'),'LRTB',1,'C',true);


////// 6


$pdf->MultiCell(180, 8,utf8_decode($personal->lugarTrabajo1),1,'J','false' );


$pdf->MultiCell(180, 8,utf8_decode($personal->lugarTrababajo2),1,'J','false' );


$pdf->MultiCell(180, 8,utf8_decode($personal->lugarTrabajo3),1,'J','false' );




////// 6

$pdf->MultiCell(180, 8,utf8_decode('DIRECCIÓN DOMICILIARIA: ').''.utf8_decode($personal->direccion),1,'J','false' );





$pdf->Cell(90,8,utf8_decode('CELULAR TITULAR: ').''.utf8_decode($personal->telefono),'LRTB',0,'',true);
////// 6

$pdf->Cell(90,8,utf8_decode('CELULAR EMERGENTE: ').''.utf8_decode($personal->telefono2),'LRTB',1,'',true);
////// 6


$pdf->MultiCell(180, 10,utf8_decode('ANTECEDENTES / SANCIONES:').' '.utf8_decode($personal->sanciones),1,'J','false' );


$pdf->MultiCell(180, 10,utf8_decode('ANTECEDENTES MEDICOS:').' '.utf8_decode($personal->medicos),1,'J','false' );


//kjhaks


//¿Solvente?

//////Datos de la Base de datos
$i=0;
$cuota = 0;
/*
foreach($datos as $dato)
{    $i++; 
  $cuota = $cuota + $dato->puntuaje;
                   

  $pdf->Cell(12,8,$i,'LRTB',0,'C',false);
  $pdf->Cell(73,8,utf8_decode(),'LRTB',0,'',false);
  $pdf->Cell(50,8,utf8_decode(),'LRTB',0,'',false);
 

  
}
*/


$pdf->Output('Club_.pdf','I'); 
/*$pdf->Multicell(185,4,utf8_decode('(**) SOLO SE CONSIDERA CARGA FAMILIAR AL CÓNYUGE, HIJOS MENORES DE EDAD, O MAYORES CON DISCAPACIDAD QUE VIVAN CON EL TITULAR, EL PUNTAJE MÁXIMO POR HIJO SERÁ DE CUATRO (04) PUNTOS; EL CARNÉ DEBERÁ ESTAR VIGENTE AL MES DE ENERO DEL AÑO DEL PROCESO E INSCRITO EN EL REGISTRO DE FAMILIA.'),'','J',false);
*/
?>