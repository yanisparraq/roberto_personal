<div class="content-wrapper-before gradient-45deg-indigo-purple"></div>
<div class="breadcrumbs-dark pb-0 pt-4" id="breadcrumbs-wrapper">
  <!-- Search for small screen-->
  <div class="container">
    <div class="row">
      <div class="col s10 m6 l6">
        <h5 class="breadcrumbs-title mt-0 mb-0"><span>Agregar Personal</span></h5>
        <ol class="breadcrumbs mb-0">
          <li class="breadcrumb-item"><a href="<?php echo base_url()?>">Home</a>


          <li class="breadcrumb-item">
              <a href="<?php echo base_url()?>grupos">
              Personal
            </a>
          </li>
            <li class="breadcrumb-item active">Agregar
            </li>
        </ol>
      </div>
      
      
    </div>
  </div>
</div>


<div class="row">
  <div class="col s12">
    <div id="input-fields" class="card card-tabs">
      <div class="card-content">
        <div class="card-title">
          <div class="row">
            <div class="col s12 m6 l10">
              <h4 class="card-title">Complete Los  Campos 


             
              </h4>
            </div>            
          </div>
        </div>
        <div id="view-input-fields">
          <div class="row">


            <?php
            $errors=validation_errors('<li>','</li>');
              if($errors!="")
              {
                  ?>

              <div class="card-alert card red lighten-5">
                <div class="card-content red-text">
                      <ul>
                          <?php echo $errors;?>
                      </ul>
                </div>
               
              </div>
                 
                  <?php
              } ?> 
              <form action="<?php echo base_url();?>personal/add" id="addUsuarios" name = "addUsuarios" method="post">
            
              <div class="row">
                <div class="input-field col s3">
                  <i class="material-icons prefix">account_box</i>
                  <input id="icon_prefix" type="text" class="validate"    name="nombre" value="<?php echo set_value_input(array(),'nombre','nombre')?>" >
                  <label for="icon_prefix">Primer Nombre</label>
                </div>
                <div class="input-field col s3">
                  <i class="material-icons prefix">account_box</i>
                  <input id="icon_prefix2" type="text"  name="nombre2"  value="<?php echo set_value_input(array(),'nombre2','nombre2')?>" >
                  <label for="icon_prefix2">Segundo Nombre</label>
                </div>

                <div class="input-field col s3">
                  <i class="material-icons prefix">account_box</i>
                  <input id="icon_prefix3" type="text"  name="apellido"  value="<?php echo set_value_input(array(),'apellido','apellido')?>" >
                  <label for="icon_prefix3">Primer Apellido</label>
                </div>

                <div class="input-field col s3">
                  <i class="material-icons prefix">account_box</i>
                  <input id="icon_prefix4" type="text"  name="apellido2"  value="<?php echo set_value_input(array(),'apellido2','apellido2')?>" >
                  <label for="icon_prefix4">Segundo Nombre</label>
                </div>


                
                <div class="input-field col s3">
                  <i class="material-icons prefix">contacts</i>
                  <input id="icon_prefix5" type="text"  name="dni"  value="<?php echo set_value_input(array(),'dni','dni')?>" >
                  <label for="icon_prefix5">DNI</label>
                </div>


                <div class="input-field col s3">
                  <i class="material-icons prefix">contacts</i>
                  <input id="icon_prefix7" type="text"  name="cip"  value="<?php echo set_value_input(array(),'cip','cip')?>" >
                  <label for="icon_prefix7">CIP</label>
                </div>


                <div class="col s3 input-field">
                <i class="material-icons prefix">contacts</i>
                    <select name="estado_civil" id="estado_civil"  >
                      <option>Selecione...</option>
                     
                      <option value="Casado">Casado</option>
                      <option value="Soltero">Soltero</option>
                      <option value="Viudo">Viudo</option>
                                
                         
                    </select>
                    <label>Estado Civil</label>
                  </div>

                  <div class="input-field col s3">
                    <i class="material-icons prefix">event_available</i>
                    <input id="icon_prefix8" type="text" class="datepicker" name="fecha_nacimiento" value="<?php echo set_value_input(array(),'fecha_nacimiento','fecha_nacimiento')?>" >
                    <label for="icon_prefix8">Fecha de Nacimiento</label>
                  </div>


                  <div class="input-field col s3">
                    <i class="material-icons prefix">face</i>
                    <input id="icon_telephone" type="number" class="validate" name="hijo" value="<?php echo set_value_input(array(),'hijo','hijo')?>">
                    <label for="icon_telephone">Hijos</label>
                  </div>



                  

                <div class="input-field col s9">
                 <i class="material-icons prefix">location_on</i>
                  <textarea id="textarea2" class="materialize-textarea" name="lugar_nacimiento"><?php echo set_value_input(array(),'lugar_nacimiento','lugar_nacimiento')?></textarea>
                  <label for="textarea2">Lugar de Nacimiento</label>
                </div>




                  <div class="input-field col s3">
                  <i class="material-icons prefix">phone</i>
                  <input id="icon_prefix9" type="text"  name="telefono"  value="<?php echo set_value_input(array(),'telefono','telefono')?>" >
                  <label for="icon_prefix9">Celular Titular</label>
                </div>


                <div class="input-field col s3">
                  <i class="material-icons prefix">phone</i>
                  <input id="icon_prefix10" type="text"  name="telefono2"  value="<?php echo set_value_input(array(),'telefono2','telefono2')?>" >
                  <label for="icon_prefix10">Celular Emergente</label>
                </div>



                <div class="input-field col s6">
                 <i class="material-icons prefix">location_on</i>
                  <textarea id="textarea24" class="materialize-textarea" name="direccion"></textarea>
                  <label for="textarea24">Direccion Domiciliaria</label>
                </div>
          





                
                <div class="col s3 input-field">
                <i class="material-icons prefix">stars</i>
                    <select name="id_grado" id="id_grado"  >
                      <option value="">Selecione...</option>
                      <?php
                      foreach($grados as $grado)
                      {                  
                    ?>
                         <option value="<?php echo $grado->id?>"><?php echo $grado->nombre?> </option>
                    <?php
                      }                  
                    ?>
                     
                                                    
                         
                    </select>
                    <label>Grados</label>
                  </div>



                  <div class="col s3 input-field">
                    <i class="material-icons prefix">business</i>
                    <select name="id_unidad" id="id_unidad"  >
                      <option>Selecione...</option>
                      <?php
                      foreach($unidades as $unidad)
                      {                  
                    ?>
                         <option value="<?php echo $unidad->id?>"><?php echo $unidad->nombre?> </option>
                    <?php
                      }                  
                    ?>
                     
                                                    
                         
                    </select>
                    <label>Unidad Origen</label>
                  </div>


                  <div class="col s3 input-field">
                    <i class="material-icons prefix">business</i>
                    <select name="id_grupo" id="id_grupo"  >
                      <option>Selecione...</option>
                      <?php
                      foreach($grupos as $grupo)
                      {                  
                    ?>
                         <option value="<?php echo $grupo->id?>"><?php echo $grupo->nombre?> </option>
                    <?php
                      }                  
                    ?>
                     
                                                    
                         
                    </select>
                    <label>Grupo Ocupacional</label>
                  </div>





                  <div class="col s3 input-field">
                    <i class="material-icons prefix">streetview</i>
                    <select name="id_especialidad" id="id_especialidad"  >
                      <option>Selecione...</option>
                      <?php
                      foreach($especialidades as $especialidades)
                      {                  
                    ?>
                         <option value="<?php echo $especialidades->id?>"><?php echo $especialidades->nombre?> </option>
                    <?php
                      }                  
                    ?>
                     
                                                    
                         
                    </select>
                    <label>Especialidades</label>
                  </div>




                  
                  <div class="col s4 input-field">
                    <i class="material-icons prefix">polymer</i>
                    <select name="condicion" id="condicion"  >
                      <option>Selecione...</option>
                   
                      <option value="CONTRATADO">CONTRATADO </option>
                      <option value="NOMBRADO">NOMBRADO </option>
                   
                     
                                                    
                         
                    </select>
                    <label>Condicion</label>
                  </div>


                

             


                <div class="input-field col s4">
                  <i class="material-icons prefix">recent_actors</i>
                  <input id="icon_prefix11" type="text" class="validate"    name="puesto" value="<?php echo set_value_input(array(),'puesto','puesto')?>" >
                  <label for="icon_prefix11">Puesto</label>
                </div>


                <div class="input-field col s4">
                    <i class="material-icons prefix">event_available</i>
                    <input id="icon_prefix14" type="text" class="datepicker" name ="fecha_contrato" value="<?php echo set_value_input(array(),'fecha_contrato','fecha_contrato')?>">
                    <label for="icon_prefix14">Fecha Contratado</label>
                  </div>


                  <div class="input-field col s4">
                    <i class="material-icons prefix">event_available</i>
                    <input id="icon_prefix15" type="text" class="datepicker" name ="fecha_nombrado" value="<?php echo set_value_input(array(),'fecha_nombrado','fecha_nombrado')?>">
                    <label for="icon_prefix15">Fecha Nombrado</label>
                  </div>



                  <div class="input-field col s4">
                    <i class="material-icons prefix">event_available</i>
                    <input id="icon_prefix30" type="text" class="datepicker" name ="fecha_ascenso" value="<?php echo set_value_input(array(),'fecha_ascenso','fecha_ascenso')?>">
                    <label for="icon_prefix30">Fecha Ascenso</label>
                  </div>





                  <div class="input-field col s8">
                  <i class="material-icons prefix">business</i>
                  <input id="icon_prefix18" type="text" class="validate"    name="lugarTrabajo1" value="<?php echo set_value_input(array(),'lugarTrabajo1','lugarTrabajo1')?>" >
                  <label for="icon_prefix18">Ultimo Lugar de Trabajo</label>
                </div>

                <div class="input-field col s4">
                    <i class="material-icons prefix">event_available</i>
                    <input id="icon_prefix19" type="text" class="datepicker" name="lugarTrabaajofecha1">
                    <label for="icon_prefix19">Fecha Ingreso Ultimo Trabajo</label>
                </div>









                <div class="input-field col s8">
                  <i class="material-icons prefix">business</i>
                  <input id="icon_prefix20" type="text" class="validate"    name="lugarTrababajo2" value="<?php echo set_value_input(array(),'lugarTrababajo2','lugarTrababajo2')?>" >
                  <label for="icon_prefix20">Penultimo Lugar de Trabajo</label>
                </div>

                <div class="input-field col s4">
                    <i class="material-icons prefix">unfold_less</i>
                    <input id="icon_prefix21" type="text" class="datepicker" name="lugarTrababajofecha2">
                    <label for="icon_prefix21">Fecha Ingreso Penultimo Trabajo</label>
                </div>





                                <div class="input-field col s8">
                  <i class="material-icons prefix">business</i>
                  <input id="icon_prefix23" type="text" class="validate"    name="lugarTrabajo3" value="<?php echo set_value_input(array(),'lugarTrabajo3','lugarTrabajo3')?>" >
                  <label for="icon_prefix23">Antepenultimo Lugar de Trabajo</label>
                </div>

                <div class="input-field col s4">
                    <i class="material-icons prefix">event_available</i>
                    <input id="icon_prefix21" type="text" class="datepicker" name="lugarTrabajajofecha3">
                    <label for="icon_prefix21">Fecha Ingreso Antepenultimo Trabajo</label>
                </div>

                <div class="input-field col s6">
                 <i class="material-icons prefix">phone</i>
                  <textarea id="textarea25" class="materialize-textarea" name="sanciones"></textarea>
                  <label for="textarea25">Antecedente/Sanciones</label>
                </div>


                <div class="input-field col s6">
                 <i class="material-icons prefix">phone</i>
                  <textarea id="textarea24" class="materialize-textarea" name="medicos"></textarea>
                  <label for="textarea24">Antecedentes Medicos</label>
                </div>


 

                <div class="input-field col s12">
                  <button class="btn waves-effect waves-light right" type="submit">Enviar
                    <i class="material-icons right">send</i>
                  </button>
                </div>
              </div>
            </form>
         
          </div>
        </div>
      </div>
    </div>
  </div>
</div>









 <!-- BEGIN: Header-->
   

    <!-- END: Footer-->
    <!-- BEGIN VENDOR JS-->
    <script src="<?php echo base_url();?>public/app-assets/js/vendors.min.js"></script>
    <!-- BEGIN VENDOR JS-->
    <!-- BEGIN PAGE VENDOR JS-->
    <!-- END PAGE VENDOR JS-->
    <!-- BEGIN THEME  JS-->
    <script src="<?php echo base_url();?>public/app-assets/js/plugins.js"></script>

    <!-- END THEME  JS-->
    <!-- BEGIN PAGE LEVEL JS-->
  
    <!-- END PAGE LEVEL JS-->
 