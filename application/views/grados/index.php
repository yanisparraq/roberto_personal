


  <div class="content-wrapper-before gradient-45deg-indigo-purple"></div>
<div class="breadcrumbs-dark pb-0 pt-4" id="breadcrumbs-wrapper">
  <!-- Search for small screen-->
  <div class="container">
    <div class="row">
      <div class="col s10 m6 l6">
        <h5 class="breadcrumbs-title mt-0 mb-0"><span>Grados</span></h5>
        <ol class="breadcrumbs mb-0">
          <li class="breadcrumb-item"><a href="<?php echo base_url()?>Home">Inicio</a>

          <li class="breadcrumb-item active">Grados
          </li>
        </ol>
      </div>

      
      <div class="col s2 m6 l6">
        <a class="btn dropdown-settings waves-effect waves-light breadcrumbs-btn right" 
          href="<?php echo base_url()?>grados/add">
          <i class='material-icons left'>add</i><span class="hide-on-small-onl">Agregar</span>
        </a>        
      </div>
      
    </div>
  </div>
</div>



<div class="col s12">
  <div class="container">
    <div class="section section-data-tables">
     

      <div class="row">
        <div class="col s12 m12 l12">
          <div id="button-trigger" class="card card card-default scrollspy">
            <div class="card-content">
              
              <div class="row">
               
                <div class="col s12">
                  <table id="data-table-simple" class="bordered striped Highlight">
                    <thead>
                      <tr>
                        <th>Nombre</th>
                        <th>Abreviatura</th>
                                             
                        <th> 
                          <div class="row">
                          <div class="col s12">Acción</div>
                           </div>
                        </th>
                       
                        
                      </tr>
                    </thead>
                    <tbody>
                    <?php
                      foreach($datos as $dato)
                      {                  
                    ?>
                      <tr>
                      <td><?php echo $dato->nombre?></td>
                      <td><?php echo $dato->abreviatura?></td>
                        
                       
                 
                      <td  class="text-center" >
                        <div class="row">
                          <div class="col s6"><a  href="<?php echo base_url()?>grados/edit/<?php echo $dato->id?>" ><i class="material-icons">edit</i></a></div>
                          <div class="col s6"><a href="<?php echo base_url()?>grados/delete/<?php echo $dato->id?>" ><i class="material-icons m-10">delete</i></a></div>
                        </div>

                        </td>

                        
                        </td>
                       
                      </tr>
                      <?php
                      }                  
                    ?>
                    </tbody>
                   
                  </table>


<?php
 echo $this->pagination->create_links()?> 
                </div>
 
  
              </div>
           
            </div>
           
          </div>
         
        </div>
      </div>
    
    </div>
  
  </div>

</div>
<?php
    if($this->session->flashdata('mensaje')!='')
    {
       ?>
      
          <script> 
      // alertas('<?php echo $this->session->flashdata('css')?>','<?php echo $this->session->flashdata('mensaje')?>');
      swal({
    title: "<?php echo $this->session->flashdata('mensaje')?> ",
    text: " ",
    icon: '<?php echo $this->session->flashdata('css')?>',
    
    timer: 2000,
    buttons: false
  }); </script>
          

      
       <?php 
    }
    ?> 








    






