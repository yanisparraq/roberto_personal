
      <p class="login-box-msg">Crear Cuenta </p>
          <form action="<?php echo base_url();?>acceso/membresias" id="menbresia" name = "menbresia" method="post">
       
        <?php
              if($this->session->flashdata('mensaje')!='')
              {
                 ?>
                 <div class="alert alert-<?php echo $this->session->flashdata('css')?>"><?php echo $this->session->flashdata('mensaje')?></div>
                 <?php 
              }
         
                  //acá visualizamos los mensajes de error
                  $errors=validation_errors('<li>','</li>');
                  if($errors!="")
                  {
                      ?>
                      <div class="alert alert-danger">
                          <ul>
                              <?php echo $errors;?>
                          </ul>
                      </div>
                      <?php
                  }
                  ?>
        <div class="input-group mb-3">
          <input type="text" class="form-control" name="nombre" placeholder="Nombre" autocomplete="off" value="<?php echo set_value_input(array(),'nombre','nombre')?>" >
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-user"></span>
            </div>
          </div>
        </div>
        <div class="input-group mb-3">
          <input type="text" class="form-control" name="apellido" placeholder="Apellido" autocomplete="off" value="<?php echo set_value_input(array(),'apellido','apellido')?>" >
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-user"></span>
            </div>
          </div>
        </div>
        <div class="input-group mb-3">
          <input type="text" class="form-control" name="cedula" placeholder="Cedula" autocomplete="off" value="<?php echo set_value_input(array(),'cedula','cedula')?>" >
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fa fa-address-card"></span>
            </div>
          </div>
        </div>
        <div class="input-group mb-3">
          <input type="text" class="form-control" name="correo2" placeholder="Email" autocomplete="off" value="<?php echo set_value_input(array(),'correo2','correo2')?>">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-envelope"></span>
            </div>
          </div>
        </div>
        <div class="input-group mb-3">
          <input type="text" class="form-control" name="telefono" placeholder="Telefono" autocomplete="off"
          value="<?php echo set_value_input(array(),'telefono','telefono')?>">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-phone"></span>
            </div>
          </div>
        </div>
        <div class="input-group mb-3">
          <input type="password" class="form-control" name="password" placeholder="Password" autocomplete="off"
          value="<?php echo set_value_input(array(),'password','password')?>">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-lock"></span>
            </div>
          </div>
        </div>

        <div class="input-group mb-3">
          <input type="password" class="form-control" name="password2" placeholder="Repita password" autocomplete="off" autocomplete="off"
          value="<?php echo set_value_input(array(),'password2','password2')?>">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-lock"></span>
            </div>
          </div>
        </div>
        
        <div class="row">
          <div class="col-8">
            <div class="icheck-primary">
              <input type="checkbox" id="agreeTerms" name="terms" value="agree">
              <label for="agreeTerms">
               Acepto <a href="#">terminos</a>
              </label>
            </div>
          </div>
          <!-- /.col -->
          <div class="col-4">
            <button type="submit" class="btn btn-primary btn-block">Registrar</button>
          </div>
          <!-- /.col -->
        </div>
      </form>

      <a href="<?php echo base_url()?>" class="text-center">Ya tengo una cuenta</a>
    </div>
  
<script>
    
$(document).ready(function() { 

    $('#menbresia').submit(function(e) {  
      alert(2222);
        e.preventDefault();
        $.ajax({
            type: "POST",
            url: '<?php echo base_url();?>/acceso/membresias',
            data: $(this).serialize(),
            success: function(result)
            {              
               $('#resultado2').html(result);
           }
       });
     });
});
</script>