                  <div class="text-center">
                    <h1 class="h4 text-gray-900 mb-4">Registro de Usuario</h1>
                  </div>
                  <?php echo form_open(null,array('name'=>'form','class'=>'user'));?>
                <?php
            if($this->session->flashdata('mensaje')!='')
            {
               ?>
               <div class="alert alert-<?php echo $this->session->flashdata('css')?>"><?php echo $this->session->flashdata('mensaje')?></div>
               <?php 
            }
            ?>
            <?php
                //acá visualizamos los mensajes de error
                $errors=validation_errors('<li>','</li>');
                if($errors!="")
                {
                    ?>
                    <div class="alert alert-danger">
                        <ul>
                            <?php echo $errors;?>
                        </ul>
                    </div>
                    <?php
                }
                ?>
                <div class="form-group">
    <div class="input-group mb-3">
       <input id="rol" name="rol" type="hidden" value="1">
       <input id="rol" name="categoria" type="hidden" value="1">
    <div class="input-group mb-3">
      <input  class="form-control form-control-user" id = "nombre" aria-describedby="emailHelp" type="text" name="nombre" placeholder="Nombre" value="<?php echo set_value_input(array(),'nombre','nombre')?>">
      <div class="input-group-append">
          <div class="input-group-text">
            <span class="far fa-id-card"></span>
          </div>
        </div>
    </div>
    <div class="input-group mb-3">
      <input  class="form-control form-control-user" id = "apellido" aria-describedby="emailHelp" type="text" name="apellido" placeholder="Apellido" value="<?php echo set_value_input(array(),'apellido','apellido')?>">
      <div class="input-group-append">
          <div class="input-group-text">
            <span class="far fa-id-card"></span>
          </div>
        </div>
    </div>    
      <input type="email" class="form-control form-control-user" name="correo" placeholder="Email"value="<?php echo set_value_input(array(),'correo','correo')?>">
        <div class="input-group-append">
          <div class="input-group-text">
            <span class="fas fa-envelope"></span>
          </div>
        </div>
      </div>
    </div>
    <div class="input-group mb-3">
      <input type="password" name="password" class="form-control form-control-user" placeholder="Contraseña" value="<?php echo set_value_input(array(),'password','password')?>" >
        <div class="input-group-append">
          <div class="input-group-text">
            <span class="fas fa-lock"></span>
          </div>
        </div>
    </div>

    <div class="input-group mb-3">
      <input type="password" name="password2" class="form-control form-control-user" placeholder="Repetir Contraseña" value="<?php echo set_value_input(array(),'password2','password2')?>" >
        <div class="input-group-append">
          <div class="input-group-text">
            <span class="fas fa-lock"></span>
          </div>
        </div>
    </div>

    <div class="input-group mb-3">
      <input  class="form-control form-control-user" id = "apellido" aria-describedby="emailHelp" type="text" name="telefono" placeholder="Telefono" value="<?php echo set_value_input(array(),'telefono','telefono')?>">
      <div class="input-group-append">
          <div class="input-group-text">
            <span class="far fas fa-phone"></span>
          </div>
        </div>
    </div>    
    <input type="submit" value="Registrar" class="btn btn-primary btn-user btn-block" />
<?php echo form_close();?>
