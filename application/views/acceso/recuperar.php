<?php echo form_open(null,array('name'=>'form','class'=>'login-form'));?>
                                      <?php
                              if($this->session->flashdata('mensaje')!='')
                              {
                                  ?>
                                  <div class="alert alert-<?php echo $this->session->flashdata('css')?>"><?php echo $this->session->flashdata('mensaje')?></div>
                                  <?php 
                              }
                          
                                  //acá visualizamos los mensajes de error
                                  $errors=validation_errors('<li>','</li>');
                                  if($errors!="")
                                  {
                                      ?>
                                      <div class="alert alert-danger">
                                          <ul>
                                              <?php echo $errors;?>
                                          </ul>
                                      </div>
                                      <?php
                                  }
                                  ?>

      <div class="row">
        <div class="input-field col s12">
          <h5 class="ml-4">Forgot Password</h5>
          <p class="ml-4">You can reset your password</p>
        </div>
      </div>
      <div class="row margin">
        <div class="input-field col s12">
        <i class="material-icons prefix pt-2">person_outline</i>
          <input id="username" type="text" name="correo" value="<?php echo set_value_input(array(),'correo','correo')?>">
          <label for="username" class="center-align">Correo</label>
        </div>
      </div>
     
   
      <div class="row">
        <div class="input-field col s12">
        <button class="btn waves-effect waves-light border-round gradient-45deg-purple-deep-orange col s12" type="submit"> Iniciar</button>
        </div>
      </div>
      <div class="row">
       
        <div class="input-field col s12 m12 l12">
          <p class="margin right-align medium-small"><a href="<?php echo base_url()?>acceso/recuperar">Recuperar Contraseña</a></p>
        </div>
      </div>
    </form>