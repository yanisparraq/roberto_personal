<div class="modal-header">
  <h5 class="modal-title">Registro Presidente de Federación</h5>
  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
    <span aria-hidden="true">&times;</span>
  </button>
</div>
<div class="modal-body">
  <form action="<?php echo base_url();?>acceso/add2" id="loginform2" method="post">
    <div class="row">
      <div class="col-sm-4">
        <div class="form-group">
          <label class="col-form-label">Nombre<span class="text-danger">*</span></label>
          <input class="form-control" type="text" name = "nombre_de_usuario" required="">
        </div>
      </div>
      <div class="col-sm-4">
        <div class="form-group">
          <label class="col-form-label">Apellido<span class="text-danger">*</span></label>
          <input class="form-control" type="text" name = "apellido" required="">
        </div>
      </div>
      <div class="col-sm-4">
        <div class="form-group">
          <label class="col-form-label">Correo<span class="text-danger">*</span></label>
          <input class="form-control" type="email" name = "correo" required="">
        </div>
      </div>
      <div class="col-sm-4">
        <div class="form-group">
          <label class="col-form-label">Telefono<span class="text-danger">*</span></label>
          <input class="form-control" type="text" name = "telefono" required="">
        </div>
      </div>




      <div class="col-sm-4">
        <div class="form-group">
          <label class="col-form-label">Zona<span class="text-danger">*</span></label>
         <select   class="form-control" name="id_zona" id="id_zona" required=""  >
            <option value="">Seleccione Zona</option>
             <?php    
              foreach($zonas as $zona)
                {
                          
                  ?>
                    <option value="<?php echo $zona->id?>"><?php echo $zona->nombre_zona?></option>
                    
                  <?php
                      
              } ?>                           
          </select>
        </div>
      </div>
          
      <div class="col-sm-4"  >
        <div class="form-group">
          <label class="col-form-label">Contraseña<span class="text-danger">*</span></label>
          <input class="form-control" type="text" name = "password" required="">
        </div>
      </div>

    </div>
    <div class="submit-section">
      <button class="btn btn-primary submit-btn">Enviar</button>
    </div>
  </form>
</div>
 <script>
 
      $(document).ready(function() { 
      
    $('#loginform4').submit(function(e) {
    
    
        e.preventDefault();
        $.ajax({
            type: "POST",
            url: '<?php echo base_url();?>acceso/add2',
            data: $(this).serialize(),

        beforeSend: function(){
           $("#edit_employee").modal("hide");
        },

          success: function (result) {         
         //  $("#exampleModal").modal("show");
         $('#resultado2').html(result);
         
         // $("#edit_employee").modal("hide");
            },
         complete: function (result) {  

         //  $("#exampleModal").modal("show");
         //$('#resultado2').html(result);
          // $("#vm_agregar_articulo").modal("hide");
        }
       });
     });
});
</script>