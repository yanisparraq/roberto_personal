<div class="content-wrapper-before gradient-45deg-indigo-purple"></div>
<div class="breadcrumbs-dark pb-0 pt-4" id="breadcrumbs-wrapper">
  <!-- Search for small screen-->
  <div class="container">
    <div class="row">
      <div class="col s10 m6 l6">
        <h5 class="breadcrumbs-title mt-0 mb-0"><span>Editar Especialidades</span></h5>
        <ol class="breadcrumbs mb-0">
          <li class="breadcrumb-item"><a href="<?php echo base_url()?>">Home</a>


          <li class="breadcrumb-item">
              <a href="<?php echo base_url()?>especialidades">
              Especialidades
            </a>
          </li>
            <li class="breadcrumb-item active">Editar
            </li>
        </ol>
      </div>
      
      
    </div>
  </div>
</div>



<div class="row">
  <div class="col s12">
    <div id="input-fields" class="card card-tabs">
      <div class="card-content">
        <div class="card-title">
          <div class="row">
            <div class="col s12 m6 l10">
              <h4 class="card-title"> Edite Los  Campos              
              </h4>
            </div>            
          </div>
        </div>
        <div id="view-input-fields">
          <div class="row">
            <?php
            $errors=validation_errors('<li>','</li>');
              if($errors!="")
              {
                  ?>

              <div class="card-alert card red lighten-5">
                <div class="card-content red-text">
                      <ul>
                          <?php echo $errors;?>
                      </ul>
                </div>
               
              </div>
                 
                  <?php
              } ?> 
              <form action="<?php echo base_url();?>especialidades/edit/<?php echo $id?>"   method="post"  enctype="multipart/form-data">
            
              <div class="row">
              <div class="input-field col s6">
                  <i class="material-icons prefix">local_library</i>
                  <input id="icon_prefix" type="text"    name="nombre" value="<?php echo set_value_input(array($dato->nombre),'nombre',$dato->nombre )?>" data-error=".errorTxt1">
                  <label for="icon_prefix">Nombre de la Especialidad</label>
                </div>



                <div class="input-field col s6">
                  <i class="material-icons prefix">pan_tool</i>
                  <input id="icon_prefix22" type="text"     name="abreviatura" value="<?php echo set_value_input(array($dato->abreviatura),'abreviatura',$dato->abreviatura)?>" data-error=".errorTxt1">
                  <label for="icon_prefix22">Abreviatura</label>
                </div>




                <div class="file-field input-field col s6">
                  <div class="btn">
                    <span>File</span>
                    <input type="file">
                  </div>
                  <div class="file-path-wrapper">
                  <input type="file" name="file" />
                    <input class="file-path validate" type="file" name="file" name="file">
                  </div>
                </div>
                <input type="hidden" id = "id" name="id"  value="<?php echo $id?>" />
                <div class="input-field col s12">
                  <button class="btn waves-effect waves-light right" type="submit" name="action">Enviar
                    <i class="material-icons right">send </i>
                  </button>
                </div>
              </div>
              </form>
         
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  </div>









 <!-- BEGIN: Header-->
   

    <!-- END: Footer-->
    <!-- BEGIN VENDOR JS-->
    <script src="<?php echo base_url();?>public/app-assets/js/vendors.min.js"></script>
    <!-- BEGIN VENDOR JS-->
    <!-- BEGIN PAGE VENDOR JS-->
    <!-- END PAGE VENDOR JS-->
    <!-- BEGIN THEME  JS-->
    <script src="<?php echo base_url();?>public/app-assets/js/plugins.js"></script>

    <!-- END THEME  JS-->
    <!-- BEGIN PAGE LEVEL JS-->
  
    <!-- END PAGE LEVEL JS-->
 









<div class="col s12">
  <div class="container">
    <div class="section section-data-tables">
     

      <div class="row">
       
      </div>
    
    </div>
  
  </div>

</div>









    








