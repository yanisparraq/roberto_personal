 
 
 
 
 <div class="content-wrapper-before gradient-45deg-indigo-purple"></div>
<div class="breadcrumbs-dark pb-0 pt-4" id="breadcrumbs-wrapper">
  <!-- Search for small screen-->
  <div class="container">
    <div class="row">
      <div class="col s10 m6 l6">
        <h5 class="breadcrumbs-title mt-0 mb-0"><span>Editar usuario</span></h5>
        <ol class="breadcrumbs mb-0">
          <li class="breadcrumb-item"><a href="<?php echo base_url()?>">Home</a>


          <li class="breadcrumb-item">
              <a href="javascript:void(0);" onclick="myFunction('<?php echo base_url()?>campos','#resultado2');">
              Campos
            </a>
          </li>
            <li class="breadcrumb-item active">Editar
            </li>
        </ol>
      </div>
      
      
    </div>
  </div>
</div>


<div class="row">
  <div class="col s12">
    <div id="input-fields" class="card card-tabs">
      <div class="card-content">
        <div class="card-title">
          <div class="row">
            <div class="col s12 m6 l10">
              <h4 class="card-title">Edite Los  Campos 


             
              </h4>
            </div>            
          </div>
        </div>
        <div id="view-input-fields">
          <div class="row">


            <?php
            $errors=validation_errors('<li>','</li>');
              if($errors!="")
              {
                  ?>

              <div class="card-alert card red lighten-5">
                <div class="card-content red-text">
                      <ul>
                          <?php echo $errors;?>
                      </ul>
                </div>
               
              </div>
                 
                  <?php
              } ?> 
              <form action="<?php echo base_url();?>usuarios/edit" id="addUsuarios" name = "addUsuarios" method="post">
            
              <div class="row">
                <div class="input-field col s6">
                  <i class="material-icons prefix">pan_tool</i>
                  <input id="icon_prefix" type="text" class="validate"    name="nombre" value="<?php echo set_value_input(array($dato->nombre),'nombre',$dato->nombre)?>" data-error=".errorTxt1">
                  <label for="icon_prefix">Primer Nombre</label>
                </div>
                <div class="input-field col s6">
                  <i class="material-icons prefix">unfold_less</i>
                  <input id="icon_telephone" type="text"  class="validate" name="nombre2"  value="<?php echo set_value_input(array($dato->nombre2),'nombre2',$dato->nombre2)?>" data-error=".errorTxt2" >
                  <label for="icon_telephone">Segundo Nombre</label>
                </div>



                <div class="input-field col s6">
                  <i class="material-icons prefix">pan_tool</i>
                  <input id="icon_prefix2" type="text" class="validate"    name="apellido" value="<?php echo set_value_input(array($dato->apellido),'apellido',$dato->apellido)?>" data-error=".errorTxt3">
                  <label for="icon_prefix2">Primer Apellido</label>
                </div>
                <div class="input-field col s6">
                  <i class="material-icons prefix">unfold_less</i>
                  <input id="icon_telephone3" type="text"  class="validate" name="apellido2"  value="<?php echo set_value_input(array($dato->apellido2),'apellido2',$dato->apellido2)?>" data-error=".errorTxt4" >
                  <label for="icon_telephone3">Segundo Nombre</label>
                </div>



                <div class="input-field col s6">
                  <i class="material-icons prefix">pan_tool</i>
                  <input id="icon_prefix4" type="text" class="validate"    name="cedula" value="<?php echo set_value_input(array($dato->cedula),'cedula',$dato->cedula)?>" data-error=".errorTxt5">
                  <label for="icon_prefix4">Cedula</label>
                </div>
                <div class="input-field col s6">
                  <i class="material-icons prefix">unfold_less</i>
                  <input id="icon_telephone5" type="text"  class="validate" name="telefono"  value="<?php echo set_value_input(array($dato->telefono),'telefono',$dato->telefono)?>" data-error=".errorTxt6" >
                  <label for="icon_telephone5">Telefono</label>
                </div>

                <div class="input-field col s6">
                  <i class="material-icons prefix">unfold_less</i>
                  <input id="icon_telephone6" type="text"  class="validate" name="correo"  value="<?php echo set_value_input(array($dato->correo),'correo',$dato->correo)?>" data-error=".errorTxt7" >
                  <label for="icon_telephone6">Correo</label>
                </div>



                <div class="input-field col s6">
                <i class="material-icons prefix">pan_tool</i>
                  <select name="id_rol" id="id_rol" >
                   
                  <option value="<?php echo $dato->id_rol;?>"><?php echo $dato->nombre_rol;?></option>
                        <?php    
                        foreach($roles as $rol)
                          {
                                    
                            ?>
                              <option value="<?php echo $rol->id?>"><?php echo $rol->nombre_rol?></option>
                              
                            <?php
                                
                        } ?>  
                      
                    
                  </select>
                  <label>Unión</label>
                </div>

            


                    <input type="hidden" id = "id" name="id"  value="<?php echo $id?>" />


                <div class="input-field col s12">
                  <button class="btn waves-effect waves-light right" type="submit" name="action">Enviar
                    <i class="material-icons right">send</i>
                  </button>
                </div>
              </div>
            </form>
         
          </div>
        </div>
      </div>
    </div>
  </div>
</div>









 <!-- BEGIN: Header-->
   

    <!-- END: Footer-->
    <!-- BEGIN VENDOR JS-->
    <script src="<?php echo base_url();?>public/app-assets/js/vendors.min.js"></script>
    <!-- BEGIN VENDOR JS-->
    <!-- BEGIN PAGE VENDOR JS-->
    <!-- END PAGE VENDOR JS-->
    <!-- BEGIN THEME  JS-->
    <script src="<?php echo base_url();?>public/app-assets/js/plugins.js"></script>

    <!-- END THEME  JS-->
    <!-- BEGIN PAGE LEVEL JS-->
  
    <!-- END PAGE LEVEL JS-->
 

<script>



$(document).ready(function() { 
    $('#addUsuarios').submit(function(e) {     
        e.preventDefault();
        $.ajax({
            type: "POST",
            url: '<?php echo base_url();?>/usuarios/edit',
            data: $(this).serialize(),
            success: function(result)
            {              
               $('#resultado2').html(result);
           }
       });
     });
});
</script>
 
 
 