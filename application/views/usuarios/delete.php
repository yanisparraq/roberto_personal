<div class="form-header">
                  <h3>Eliminar Usuarios</h3>
                  <p>¿Está seguro de que desea eliminar?</p>
                </div>
                <div class="modal-btn delete-action">
                  <div class="row">
                    <div class="col-6">
                        <a href="javascript:void(0);" onclick="eliminar('<?php echo base_url()?>usuarios/delete/<?php echo $id?>')" data-dismiss="modal" class="btn btn-primary continue-btn">Borrar</a>
                    </div>
                    <div class="col-6">
                      <a href="javascript:void(0);" data-dismiss="modal" class="btn btn-primary cancel-btn">Cancelar</a>
                    </div>
                  </div>
                </div>