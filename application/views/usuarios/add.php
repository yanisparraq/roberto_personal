<div class="content-wrapper-before gradient-45deg-indigo-purple"></div>
<div class="breadcrumbs-dark pb-0 pt-4" id="breadcrumbs-wrapper">
  <!-- Search for small screen-->
  <div class="container">
    <div class="row">
      <div class="col s10 m6 l6">
        <h5 class="breadcrumbs-title mt-0 mb-0"><span>Agregar Usuarios</span></h5>
        <ol class="breadcrumbs mb-0">
          <li class="breadcrumb-item"><a href="<?php echo base_url()?>">Home</a>


          <li class="breadcrumb-item">
              <a href="javascript:void(0);" onclick="myFunction('<?php echo base_url()?>usuarios','#resultado2');">
              Usuarios
            </a>
          </li>
            <li class="breadcrumb-item active">Agregar
            </li>
        </ol>
      </div>
      
      
    </div>
  </div>
</div>


<div class="row">
  <div class="col s12">
    <div id="input-fields" class="card card-tabs">
      <div class="card-content">
        <div class="card-title">
          <div class="row">
            <div class="col s12 m6 l10">
              <h4 class="card-title">Complete Los  Campos 


             
              </h4>
            </div>            
          </div>
        </div>
        <div id="view-input-fields">
          <div class="row">


            <?php
            $errors=validation_errors('<li>','</li>');
              if($errors!="")
              {
                  ?>

              <div class="card-alert card red lighten-5">
                <div class="card-content red-text">
                      <ul>
                          <?php echo $errors;?>
                      </ul>
                </div>
               
              </div>
                 
                  <?php
              } ?> 
              <form action="<?php echo base_url();?>usuarios/add" id="addUsuarios" name = "addUsuarios" method="post">
            
              <div class="row">




              
                <div class="input-field col s6">
                    <i class="material-icons prefix">contacts</i>
                    <input id="icon_prefix" type="text"   name="nombre" value="<?php echo set_value_input(array(),'nombre','nombre')?>"  required >
                    <label for="icon_prefix">Primer Nombre</label>
                </div>


                <div class="input-field col s6">
                    <i class="material-icons prefix">contacts</i>
                    <input id="icon_prefix2" type="text"   name="nombre2" value="<?php echo set_value_input(array(),'nombre2','nombre2')?>" data-error=".errorTxt2"  >
                    <label for="icon_prefix2">Segundo Nombre</label>
                </div>

                
                <div class="input-field col s6">
                    <i class="material-icons prefix">contacts</i>
                    <input id="icon_prefix3" type="text"   name="apellido" value="<?php echo set_value_input(array(),'apellido','apellido')?>" data-error=".errorTxt3" required >
                    <label for="icon_prefix3">Primer Apellido</label>
                </div>


                <div class="input-field col s6">
                    <i class="material-icons prefix">contacts</i>
                    <input id="icon_prefix4" type="text"   name="apellido2" value="<?php echo set_value_input(array(),'apellido2','apellido2')?>" data-error=".errorTxt4" >
                    <label for="icon_prefix4">Segundo Apellido</label>
                </div>

                <div class="input-field col s6">
                <i class="fas fa-address-card prefix"></i>
                  
                    <input id="icon_prefix5" type="text"   name="cedula" value="<?php echo set_value_input(array(),'cedula','cedula')?>" data-error=".errorTxt5" required >
                    <label for="icon_prefix5">Cedula</label>
                </div>
                
                <div class="input-field col s6">
                <i class="material-icons prefix">phone</i>
                  
                    <input id="icon_prefix6" type="text"   name="telefono" value="<?php echo set_value_input(array(),'telefono','telefono')?>" data-error=".errorTxt6" required >
                    <label for="icon_prefix6">Telefono</label>
                </div>

                <div class="input-field col s6">
                <i class="material-icons prefix">email</i>
                  
                    <input id="icon_prefix7" type="text"   name="correo" value="<?php echo set_value_input(array(),'correo','correo')?>" data-error=".errorTxt7" required >
                    <label for="icon_prefix7">Correo</label>
                </div>
                



                <div class="col s6 input-field">
                    <select name="id_rol" id="id_rol" required >
                      <option>Selecione Rol</option>
                      <?php    
                          foreach($roles as $rol)
                            {
                                      
                              ?>
                                <option value="<?php echo $rol->id?>"><?php echo $rol->nombre_rol?></option>
                                
                              <?php
                                  
                          } ?>  
                    </select>
                    <label>Rol</label>
                  </div>
                  <div class="input-field col s12">
                  <i class="material-icons prefix">fingerprint</i>
                    <input id="password" type="password" name= "password" class="validate" required>
                    <label for="password">Password</label>
                  </div>
                  

                  <div class="input-field col s12">
                    <button class="btn waves-effect waves-light right" type="submit" name="action">Enviar
                      <i class="material-icons right">send</i>
                    </button>
                  </div>



          




              </div>
            </form>
         
          </div>
        </div>
      </div>
    </div>
  </div>  
</div>








<script src="<?php echo base_url();?>public/app-assets/js/vendors.min.js"></script>
    <!-- BEGIN VENDOR JS-->
    <!-- BEGIN PAGE VENDOR JS-->
    <!-- END PAGE VENDOR JS-->
    <!-- BEGIN THEME  JS-->
    <script src="<?php echo base_url();?>public/app-assets/js/plugins.js"></script>


<script>



$(document).ready(function() { 
    $('#addUsuarios').submit(function(e) {     
        e.preventDefault();
        $.ajax({
            type: "POST",
            url: '<?php echo base_url();?>/usuarios/add',
            data: $(this).serialize(),
            success: function(result)
            {              
               $('#resultado2').html(result);
           }
       });
     });
});
</script>


