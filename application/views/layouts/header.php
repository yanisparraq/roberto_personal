<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<!-- BEGIN: Head-->

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="description" content="Materialize is a Material Design Admin Template,It's modern, responsive and based on Material Design by Google.">
    <meta name="keywords" content="materialize, admin template, dashboard template, flat admin template, responsive admin template, eCommerce dashboard, analytic dashboard">
    <meta name="author" content="ThemeSelect">
    <title>Home | <?php echo $data['title'];?></title>
    <link rel="apple-touch-icon" href="<?php echo base_url();?>public/app-assets/images/favicon/apple-touch-icon-152x152.png">
    <link rel="shortcut icon" type="image/x-icon" href="<?php echo base_url();?>public/app-assets/images/favicon/favicon-32x32.png">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!-- BEGIN: VENDOR CSS-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>public/app-assets/vendors/vendors.min.css">



    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>public/app-assets/vendors/flag-icon/css/flag-icon.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>public/app-assets/vendors/magnific-popup/magnific-popup.css">
   



    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>public/app-assets/vendors/animate-css/animate.css">
    <!-- END: VENDOR CSS-->
    <!-- BEGIN: Page Level CSS-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>public/app-assets/css/themes/horizontal-menu-template/materialize.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>public/app-assets/css/themes/horizontal-menu-template/style.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>public/app-assets/css/layouts/style-horizontal.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>public/app-assets/css/pages/dashboard.css">
    <!-- END: Page Level CSS-->
    <!-- BEGIN: Custom CSS-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>public/app-assets/css/custom/custom.css">
    
    
    
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    
    
    <!-- END: Custom CSS-->
</head>
<!-- END: Head-->

   
<header class="page-topbar" id="header">
    <div class="navbar navbar-fixed">
        <nav class="navbar-main navbar-color nav-collapsible sideNav-lock navbar-dark gradient-45deg-light-blue-cyan">
            <div class="nav-wrapper">
                <ul class="left">
                    <li>
                        <h1 class="logo-wrapper"><a class="brand-logo darken-1 modal-trigger" href="#modal1"><img src="<?php echo base_url();?><?php echo $home1[10][0]  ?>  " alt="logo"><span class="logo-text hide-on-med-and-down"><?php echo $home1[10][1]  ?></span></a></h1>
                    </li>
                </ul>
                <ul class="navbar-list right">

                    <li><a class="waves-effect waves-block waves-light profile-button" href="<?php echo base_url()?>" >Inicio</a></li>
                    <li><a class="waves-effect waves-block waves-light profile-button" href="javascript:void(0);" >Radio</a></li>
                </ul>
            </div>
        </nav>
    </div>
</header>
