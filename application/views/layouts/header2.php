<!DOCTYPE html>
<!--
Template Name: Materialize - Material Design Admin Template
Author: PixInvent
Website: http://www.pixinvent.com/
Contact: hello@pixinvent.com
Follow: www.twitter.com/pixinvents
Like: www.facebook.com/pixinvents
Purchase: https://themeforest.net/item/materialize-material-design-admin-template/11446068?ref=pixinvent
Renew Support: https://themeforest.net/item/materialize-material-design-admin-template/11446068?ref=pixinvent
License: You must have a valid license purchased only from themeforest(the above link) in order to legally use the theme for your project.

-->
<html class="loading" lang="en" data-textdirection="ltr">
  <!-- BEGIN: Head-->
  <head>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="description" content="Materialize is a Material Design Admin Template,It's modern, responsive and based on Material Design by Google.">
    <meta name="keywords" content="materialize, admin template, dashboard template, flat admin template, responsive admin template, eCommerce dashboard, analytic dashboard">
    <meta name="author" content="ThemeSelect">
    <title>Users edit | Materialize - Material Design Admin Template</title>
    
    <link rel="apple-touch-icon" href="<?php echo base_url();?>public/app-assets/images/favicon/apple-touch-icon-152x152.png">
    <link rel="shortcut icon" type="image/x-icon" href="<?php echo base_url();?>public/app-assets/images/favicon/favicon-32x32.png">
    
    <!-- BEGIN: VENDOR CSS-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>public/app-assets/vendors/vendors.min.css">
    <link rel="stylesheet" href="<?php echo base_url();?>public/app-assets/vendors/select2/select2.min.css" type="text/css">
    <link rel="stylesheet" href="<?php echo base_url();?>public/app-assets/vendors/select2/select2-materialize.css" type="text/css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>public/app-assets/vendors/sweetalert/sweetalert.css">
    <!-- END: VENDOR CSS-->
    <!-- BEGIN: Page Level CSS-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>public/app-assets/css/themes/vertical-modern-menu-template/materialize.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>public/app-assets/css/themes/vertical-modern-menu-template/style.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>public/app-assets/css/pages/page-users.css">
    <!-- END: Page Level CSS-->
    <!-- BEGIN: Custom CSS-->

    

    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>public/app-assets/css/custom/custom.css">
   
    <script src='https://kit.fontawesome.com/a076d05399.js' crossorigin='anonymous'></script>
    <!-- END: Custom CSS-->
  </head>
  </head>
  <!-- END: Head-->
  <body class="vertical-layout vertical-menu-collapsible page-header-dark vertical-modern-menu preload-transitions 2-columns  app-page " data-open="click" data-menu="vertical-modern-menu" data-col="2-columns">

    <!-- BEGIN: Header-->
    <header class="page-topbar" id="header">
      <div class="navbar navbar-fixed"> 
        <nav class="navbar-main navbar-color nav-collapsible sideNav-lock navbar-dark gradient-45deg-indigo-purple no-shadow">
          <div class="nav-wrapper">
   
            <ul class="navbar-list right">
              <li class="hide-on-med-and-down"><a class="waves-effect waves-block waves-light toggle-fullscreen" href="javascript:void(0);"><i class="material-icons">settings_overscan</i></a></li>
              <li class="hide-on-large-only search-input-wrapper"><a class="waves-effect waves-block waves-light search-button" href="javascript:void(0);"><i class="material-icons">search</i></a></li>
              <li><a class="waves-effect waves-block waves-light profile-button" href="javascript:void(0);" data-target="profile-dropdown"><span class="avatar-status avatar-online"><img src="<?php echo base_url();?>public/app-assets/images/logo/materialize-logo-color.png" alt="avatar"><i></i></span></a></li>
              <li><a class="waves-effect waves-block waves-light sidenav-trigger" href="#" data-target="slide-out-right"><i class="material-icons">format_indent_increase</i></a></li>
            </ul>
            <!-- translation-button-->
         
            <!-- notifications-dropdown-->
            
            <!-- profile-dropdown-->
            <ul class="dropdown-content" id="profile-dropdown">
              <li><a class="grey-text text-darken-1" href="user-profile-page.html"><i class="material-icons">person_outline</i> Profile</a></li>
              <li><a class="grey-text text-darken-1" href="app-chat.html"><i class="material-icons">chat_bubble_outline</i> Chat</a></li>
              <li><a class="grey-text text-darken-1" href="page-faq.html"><i class="material-icons">help_outline</i> Help</a></li>
              <li class="divider"></li>
              <li><a class="grey-text text-darken-1" href="user-lock-screen.html"><i class="material-icons">lock_outline</i> Lock</a></li>
              <li><a class="grey-text text-darken-1" href="<?php echo base_url()?>acceso/salir"><i class="material-icons">keyboard_tab</i> Salir</a></li>
            </ul>
          </div>
          
        </nav>
      </div>
    </header>
    <!-- END: Header-->
    


    <!-- BEGIN: SideNav-->
    <aside class="sidenav-main nav-expanded nav-lock nav-collapsible sidenav-light sidenav-active-square">
      <div class="brand-sidebar">
        <h1 class="logo-wrapper"><a class="brand-logo darken-1" href="index.html"><img class="hide-on-med-and-down" src="<?php echo base_url();?>public/app-assets/images/logo/materialize-logo-color.png" alt="materialize logo"/><img class="show-on-medium-and-down hide-on-med-and-up" src="<?php echo base_url();?>public/app-assets/images/logo/materialize-logo.png" alt="materialize logo"/><span class="logo-text hide-on-med-and-down">MVY</span></a><a class="navbar-toggler" href="#"><i class="material-icons">radio_button_checked</i></a></h1>
      </div>
      <ul class="sidenav sidenav-collapsible leftside-navigation collapsible sidenav-fixed menu-shadow" id="slide-out" data-menu="menu-navigation" data-collapsible="menu-accordion">
       
        <li class="active bold"><a class="waves-effect waves-cyan active " href="<?php echo base_url()?>"><i class="material-icons">mail_outline</i><span class="menu-title" data-i18n="Mail">Home</span></a>
        </li>
        <li class="bold">
          <a class="waves-effect waves-cyan "  href="<?php echo base_url()?>grados">
            <i class="material-icons">pan_tool</i>
            <span class="menu-title" data-i18n="Chat">Grados</span>
          </a>
        </li>


        <li class="bold">
          <a class="waves-effect waves-cyan "  href="<?php echo base_url()?>unidades" >
            <i class="material-icons">business</i>
            <span class="menu-title" data-i18n="Chat">Unidadad Origen</span>
          </a>
        </li>


        <li class="bold">
          <a class="waves-effect waves-cyan "  href="<?php echo base_url()?>grupos" >
            <i class="fas fa-place-of-worship"></i>
            <span class="menu-title" data-i18n="Chat">Grupos Ocupacionles</span>
          </a>
        </li>

                
        <li class="bold">
          <a class="waves-effect waves-cyan "  href="<?php echo base_url()?>especialidades/" >
          <i class="material-icons">account_circle</i> 
        
            <span class="menu-title" data-i18n="Chat">Especialidades</span>
          </a>
        </li>
        

        
        


        <li class="bold">
          <a class="waves-effect waves-cyan "  href="<?php echo base_url()?>personal/" >
          <i class="fas fa-head-side-cough"></i> 
        
            <span class="menu-title" data-i18n="Chat">Personal</span>
          </a>
        </li>   


        <li class="bold">
          <a class="waves-effect waves-cyan "  href="javascript:void(0);" onclick="myFunction('<?php echo base_url()?>usuarios/','#resultado2');">
          <i class="material-icons">account_circle</i> 
        
            <span class="menu-title" data-i18n="Chat">Usuarios</span>
          </a>
        </li> 



       
      </ul>
      <div class="navigation-background"></div><a class="sidenav-trigger btn-sidenav-toggle btn-floating btn-medium waves-effect waves-light hide-on-large-only" href="#" data-target="slide-out"><i class="material-icons">menu</i></a>
    </aside>
    <!-- END: SideNav-->

    <!-- BEGIN: Page Main-->
    <div id="main">

      <div class="row" id="resultado2">
      
          <!-- Page Heading -->
           <?php // echo $content_for_layout;?>        
     


  
<!--/ Theme Customizer -->

 
    <!-- BEGIN: Footer-->

    