</div>
    </div>
<footer class="page-footer footer footer-static footer-dark gradient-45deg-indigo-purple gradient-shadow navbar-border navbar-shadow">
      <div class="footer-copyright">
        <div class="container"><span>&copy; 2023          <a href="" target="_blank">YanisComputer</a> All rights reserved.</span><span class="right hide-on-small-only">Diseñado y Desarrollado por <a href="#">Yanis Parra</a></span></div>
      </div>
    </footer>

    <!-- END: Footer-->
    <!-- BEGIN VENDOR JS-->
    <script src="<?php echo base_url();?>public/app-assets/js/vendors.min.js"></script>
    <!-- BEGIN VENDOR JS-->
    <!-- BEGIN PAGE VENDOR JS-->
    <script src="<?php echo base_url();?>public/app-assets/vendors/select2/select2.full.min.js"></script>
    <script src="<?php echo base_url();?>public/app-assets/vendors/jquery-validation/jquery.validate.min.js">   </script>
    <!-- END PAGE VENDOR JS-->
    <!-- BEGIN THEME  JS-->
    <script src="<?php echo base_url();?>public/app-assets/js/plugins.js"></script>
    <script src="<?php echo base_url();?>public/app-assets/js/search.js"></script>
    <script src="<?php echo base_url();?>public/app-assets/js/custom/custom-script.js"></script>
    <script src="<?php echo base_url();?>public/app-assets/js/scripts/customizer.js"></script>
    <!-- END THEME  JS-->
    <!-- BEGIN PAGE LEVEL JS-->
    <script src="<?php echo base_url();?>public/app-assets/js/scripts/page-users.js"></script>
    <!-- END PAGE LEVEL JS-->

    <script src="<?php echo base_url();?>public/app-assets/js/scripts/page-users.js"></script>
    <script src="<?php echo base_url();?>public/app-assets/vendors/sweetalert/sweetalert.min.js"></script>
    
    <script src="<?php echo base_url();?>public/app-assets/js/scripts/extra-components-sweetalert.js"></script>
    <script type="text/javascript" src="<?php echo base_url()?>public/js/funciones.js"></script>

    <script type="text/javascript" src="<?php echo base_url()?>public/js/ajax.js"></script>
    <!-- END PAGE LEVEL JS-->
  </body>
</html>