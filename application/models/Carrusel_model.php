<?php
class Carrusel_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }
    
    
        public function getTodos()
    {
        $query=$this->db
                ->select("*")
                ->from("carrusel")
               
                ->get();
        //echo $this->db->last_query();exit;        
        return $query->result();            
    }


   


        public function getTodosId($id)
    {
        $query=$this->db
                ->select("*")
                       
                        ->from("carrusel")  
 
                ->get();
        //echo $this->db->last_query();exit;        
     return $query->row();         
    }

    public function getid($id)
    {
        $query=$this->db
                ->select("*")
                ->from("carrusel")
               // ->order_by("nombre_rol","desc")
                 ->where(array("id"=>$id))
                ->get();
        //echo $this->db->last_query();exit;        
       return $query->row();         
    }



    public function insertar($data=array())
    {
        $this->db->insert('carrusel',$data);
        return $this->db->insert_id();
    }
    public function delete($id)
    {
        $this->db->where('id',$id);
        $this->db->delete('carrusel');
    }
    public function update($data=array(),$id)
    {
        $this->db->where('id',$id);
        $this->db->update('carrusel',$data);
    }


    public function getTodosPaginacion($pagina,$porpagina,$quehago)
    {
        switch($quehago)
        {
            case 'limit':
                $query=$this->db
                        ->select("*")
                        ->from("carrusel")   
                                               
                        ->limit($porpagina,$pagina)
                        ->order_by("id","ASD")
                        
                        ->get();
                return $query->result_array();        
            break;
            case 'cuantos':
                $query=$this->db
                        ->select("usuarios.id")
                        ->from("usuarios")   
                       
                                                   
                        ->count_all_results();
                return $query;
            break;
        }
    }

    public function getTodosPaginacion2($pagina,$porpagina,$quehago)
    {
        switch($quehago)
        {
            case 'limit':
                $query=$this->db
                        ->select("*")
                        ->from("carrusel")   
                                               
                        ->limit($porpagina,$pagina)
                        ->order_by("id","ASD")
                        
                        ->get();
                return $query->result();        
            break;
            case 'cuantos':
                $query=$this->db
                        ->select("usuarios.id")
                        ->from("usuarios")   
                       
                                                   
                        ->count_all_results();
                return $query;
            break;
        }
    }

}
