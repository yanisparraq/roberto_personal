<?php
class Personal_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }
    
    
        public function getTodos()
    {
        $query=$this->db
                ->select("*")
                ->from("personal")
               
                ->get();
        //echo $this->db->last_query();exit;        
        return $query->result();            
    }


   


        public function getTodosId($id)
    {
        $query=$this->db
                ->select("*")
                       
                        ->from("personal")  
 
                ->get();
        //echo $this->db->last_query();exit;        
     return $query->row();         
    }

    public function getid($id)
    {
        $query=$this->db
                ->select("personal.*,
                            grados.nombre as nombre_grado,
                            grupos.nombre as nombre_ocupacional,
                            especialidades.nombre as nombre_especialidad,
                            unidades.nombre as nombre_unidad ")
                ->from("personal")
                ->join("grados", "personal.id_grado=grados.id")
                ->join("grupos", "personal.id_grupo=grupos.id") 
                ->join("especialidades", "personal.id_especialidad=especialidades.id")
                ->join("unidades", "personal.id_unidad=unidades.id") 
               // ->order_by("nombre_rol","desc")
                 ->where(array("personal.id"=>$id))
                ->get();
        //echo $this->db->last_query();exit;        
       return $query->row();         
    }



    public function insertar($data=array())
    {
        $this->db->insert('personal',$data);
        return $this->db->insert_id();
    }
    public function delete($id)
    {
        $this->db->where('id',$id);
        $this->db->delete('personal');
    }
    public function update($data=array(),$id)
    {
        $this->db->where('id',$id);
        $this->db->update('personal',$data);
    }


    public function getTodosPaginacion($pagina,$porpagina,$quehago)
    {
        switch($quehago)
        {
            case 'limit':
                $query=$this->db
                        ->select("*")
                        ->from("personal")   
                                               
                        ->limit($porpagina,$pagina)
                        ->order_by("id","ASD")
                        
                        ->get();
                return $query->result_array();        
            break;
            case 'cuantos':
                $query=$this->db
                        ->select("usuarios.id")
                        ->from("usuarios")   
                       
                                                   
                        ->count_all_results();
                return $query;
            break;
        }
    }

    public function getTodosPaginacion2($pagina,$porpagina,$quehago)
    {
        switch($quehago)
        {
            case 'limit':
                $query=$this->db
                        ->select("*")
                        ->from("personal")   
                                               
                        ->limit($porpagina,$pagina)
                        ->order_by("id","ASD")
                        
                        ->get();
                return $query->result();        
            break;
            case 'cuantos':
                $query=$this->db
                        ->select("usuarios.id")
                        ->from("usuarios")   
                       
                                                   
                        ->count_all_results();
                return $query;
            break;
        }
    }

}
