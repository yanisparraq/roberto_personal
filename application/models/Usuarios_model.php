<?php
class usuarios_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }
    public function insertar($data=array())
    {
        $this->db->insert('usuarios',$data);
        return $this->db->insert_id();
    }







    public function getLogin($correo,$pass)
    {
        $query=$this->db
                ->select("id,id_rol,correo,clave_usuario,tipo_usuario_id")
                ->from("usuarios")
                ->where(array('correo'=>$correo,'clave_usuario'=>$pass))
                ->get();
         //echo $this->db->last_query();exit;        
        return $query->row();            
    }
    public function getContrasena($correo)
    {
        $query=$this->db
                ->select("clave_usuario")
                ->from("usuarios")
                ->where(array('correo'=>$correo))
                ->get();
         //echo $this->db->last_query();exit;        
        return $query->row();            
    }


    public function getLogin2($correo,$pass)
    {
        $query=$this->db
                ->select("id,id_rol,nombre,correo,clave_usuario,tipo_usuario_id")
                ->from("usuarios")
                ->where(array('correo'=>$correo,'clave_usuario'=>$pass))
                ->get();
        //echo $this->db->last_query();exit;        
        return $query->row();            
    }





    public function getTodosPaginacion($pagina,$porpagina,$quehago)
    {
        switch($quehago)
        {
            case 'limit':
                $query=$this->db
                        ->select("*,usuarios.id
                       ")
                        ->from("usuarios")   
                                                ->join("tipo_usuario", "usuarios.tipo_usuario_id=tipo_usuario.id")
                         ->join("rol", "usuarios.id_rol=rol.id") 
                     
                        ->limit($porpagina,$pagina)
                        ->order_by("nombre","ASD")
                        
                        ->get();
                return $query->result();        
            break;
            case 'cuantos':
                $query=$this->db
                        ->select("*,usuarios.id")
                        ->from("usuarios")   
                         ->join("tipo_usuario", "usuarios.tipo_usuario_id=tipo_usuario.id")
                         ->join("rol", "usuarios.id_rol=rol.id") 
                       
                                                   
                        ->count_all_results();
                return $query;
            break;
        }
    }






     public function getTodosPaginacionPacientes($pagina,$porpagina,$quehago)
    {
        switch($quehago)
        {
            case 'limit':
                $query=$this->db
                        ->select("*,usuarios.id,
                       ")
                        ->from("usuarios")   
                                                ->join("tipo_usuario", "usuarios.tipo_usuario_id=tipo_usuario.id")
                         ->join("rol", "usuarios.id_rol=rol.id") 
                       
              
                        ->limit($porpagina,$pagina)
                         ->where(array("id_rol"=>4))
                        ->order_by("nombre","ASD")
                        
                        ->get();
                return $query->result();        
            break;
            case 'cuantos':
                $query=$this->db
                        ->select("*,usuarios.id")
                        ->from("usuarios")   
                         ->join("tipo_usuario", "usuarios.tipo_usuario_id=tipo_usuario.id")
                         ->join("rol", "usuarios.id_rol=rol.id") 
                         
                                                   
                        ->count_all_results();
                return $query;
            break;
        }
    }



    public function getTodosPorId($id)
    {
        $query=$this->db
                ->select("*, usuarios.id as id")
                ->from("usuarios") 
                ->join("rol", "usuarios.id_rol=rol.id")             
                                       
                
                ->where(array("usuarios.id"=>$id))

                ->get();
        //echo $this->db->last_query();exit;        
        return $query->row();            
    }
   
     public function getDoctor()
    {
        $query=$this->db
                ->select("*")
                ->from("usuarios")              
                                       
                
                ->where(array("id_rol"=>2))

           //     ->get();
        //echo $this->db->last_query();exit;        
       // return $query->row();     
            ->get();
                return $query->result();         
    }
    public function getTodosPorId3($id)
    {
        $query=$this->db
                ->select("*,usuarios.id as id")
                ->from("usuarios")              
                ->join("tipo_usuario", "usuarios.tipo_usuario_id=tipo_usuario.id")
               // ->join("rol", "usuarios.id_rol=rol.id")
                ->where(array("usuarios.id"=>$id))
               // ->where('id_rol >', 2)
               // ->where('registro !=', 'Entrada')
                ->get();
        //echo $this->db->last_query();exit;        
        return $query->num_rows();            
    }


 
    public function delete($id)
    {
        $this->db->where('id',$id);
        $this->db->delete('usuarios');
    }
    public function update($data=array(),$id)
    {
        $this->db->where('id',$id);
        $this->db->update('usuarios',$data);
    }
    
    public function Cantidad()
    {
        $query=$this->db
              ->select("id")
                ->from("usuarios")
                ->where('id_rol',4)        
                //->get();
        //echo $this->db->last_query();exit;        
         ->count_all_results();    
          return $query;      
    }
   
    public function auto_complete($id)
    {
        $query=$this->db
                ->select("*")
                ->from("usuarios") 
               
                    ->like('cedula',$id,'after')                   
                    ->or_like('nombre',$id,'after')
                    ->or_like('apellido',$id,'after')                    
                
                   
                ->get();
          
        return $query->result();
    }


}

