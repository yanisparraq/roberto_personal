<?php
class Grupos_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }
    
    
        public function getTodos()
    {
        $query=$this->db
                ->select("*")
                ->from("grupos")
               
                ->get();
        //echo $this->db->last_query();exit;        
        return $query->result();            
    }


   


        public function getTodosId($id)
    {
        $query=$this->db
                ->select("*")
                       
                        ->from("grupos")  
 
                ->get();
        //echo $this->db->last_query();exit;        
     return $query->row();         
    }

    public function getid($id)
    {
        $query=$this->db
                ->select("*")
                ->from("grupos")
               // ->order_by("nombre_rol","desc")
                 ->where(array("id"=>$id))
                ->get();
        //echo $this->db->last_query();exit;        
       return $query->row();         
    }



    public function insertar($data=array())
    {
        $this->db->insert('grupos',$data);
        return $this->db->insert_id();
    }
    public function delete($id)
    {
        $this->db->where('id',$id);
        $this->db->delete('grupos');
    }
    public function update($data=array(),$id)
    {
        $this->db->where('id',$id);
        $this->db->update('grupos',$data);
    }


    public function getTodosPaginacion($pagina,$porpagina,$quehago)
    {
        switch($quehago)
        {
            case 'limit':
                $query=$this->db
                        ->select("*")
                        ->from("grupos")   
                                               
                        ->limit($porpagina,$pagina)
                        ->order_by("id","ASD")
                        
                        ->get();
                return $query->result_array();        
            break;
            case 'cuantos':
                $query=$this->db
                        ->select("usuarios.id")
                        ->from("usuarios")   
                       
                                                   
                        ->count_all_results();
                return $query;
            break;
        }
    }

    public function getTodosPaginacion2($pagina,$porpagina,$quehago)
    {
        switch($quehago)
        {
            case 'limit':
                $query=$this->db
                        ->select("*")
                        ->from("grupos")   
                                               
                        ->limit($porpagina,$pagina)
                        ->order_by("id","ASD")
                        
                        ->get();
                return $query->result();        
            break;
            case 'cuantos':
                $query=$this->db
                        ->select("usuarios.id")
                        ->from("usuarios")   
                       
                                                   
                        ->count_all_results();
                return $query;
            break;
        }
    }

}
