<?php
/**
 * Reglas de validacion para formularios
 */
$config = array(
       
        /**
         * add_formulario
         * */
        'unidades'
        => array(
            
            array('field' => 'nombre',
                'label' => 'Nombre de La Unidad',
                'rules' => 'required|is_string|trim'),

             array('field' => 'abreviatura',
                'label' => 'Abreviatura de la Unidad',
                'rules' => 'required|is_string|trim'),
        
            
        ),

        'grados'
        => array(
            
            array('field' => 'nombre',
                'label' => 'Nombre de La Unidad',
                'rules' => 'required|is_string|trim'),

             array('field' => 'abreviatura',
                'label' => 'Abreviatura de la Unidad',
                'rules' => 'required|is_string|trim'),
        
            
        ),



        'grupos'
        => array(
            
            array('field' => 'nombre',
                'label' => 'Nombre de La Unidad',
                'rules' => 'required|is_string|trim'),

             array('field' => 'abreviatura',
                'label' => 'Abreviatura de la Unidad',
                'rules' => 'required|is_string|trim'),
        
            
        ),


        


        'servicios'
        => array(
            
            array('field' => 'nombre',
                'label' => 'Nombre del Servicio',
                'rules' => 'required|is_string|trim'),

             array('field' => 'abreviatura',
                'label' => 'Abreviatura del Servicio',
                'rules' => 'required|is_string|trim'),
        
            
        ),


        'servicio_doc'
        => array(
            
            array('field' => 'id_usuario',
                'label' => 'Doctor',
                'rules' => 'required|is_string|trim'),

             array('field' => 'id_servicio_usuario',
                'label' => 'Doctor',
                'rules' => 'required|is_string|trim|max_length[100]|is_unique[servicio_docto.id_servicio_usuario]'),
        
            
        ),





        'carrusel'
        => array(


            array('field' => 'titulo',
            'label' => 'Titulo',
            'rules' => 'required'),
    
            
        ),



        'programaciones'
        => array(


            array('field' => 'dia',
            'label' => 'Dia',
            'rules' => 'required'),
    
            
        ),





        'pie'
        => array(


            array('field' => 'informacion',
            'label' => 'información',
            'rules' => 'required'),
    
            
        ),

        'iframe'
        => array(


            array('field' => 'contenido',
            'label' => 'Contenido',
            'rules' => 'required'),
    
            
        ),




        'ca'
        => array(
            
            array('field' => 'url',
                'label' => 'Url',
                'rules' => 'required|valid_url'),

            
        ),

        'sucursales'
        => array(
            
           
             array('field' => 'nombre_sucursal',
                'label' => 'Nombre de Sucursal',
                'rules' => 'required|is_string|trim'),

             array('field' => 'abreviaruta_sucursal',
                'label' => 'Abreviatura',
                'rules' => 'required|is_string|trim'),

            array('field' => 'Dirección',
                'label' => 'Dirección',
                'rules' => 'required|is_string|trim'),
        ),



        'especialidades'
        => array(
            
            array('field' => 'nombre',
                'label' => 'Nombre de la Especialidad',
                'rules' => 'required|is_string|trim|max_length[100]'),
            array('field' => 'abreviatura',
                'label' => 'Abreviaruta de la Especialidad',
                'rules' => 'required|is_string|trim'),
        ),

        'especialidades2'
        => array(
            
            array('field' => 'Nombre_especialidad',
                'label' => 'Nombre de la Especialidad',
                'rules' => 'required|is_string|trim|max_length[100]'),
            array('field' => 'Abreviaruta',
                'label' => 'Abreviaruta',
                'rules' => 'required|is_string|trim'),
        ),         
        
        /**
         * add_producto
         * */
   
    /**
         * login
         * */
        'loginform'
        => array(
            
           array('field' => 'correo','label' => 'Correo ','rules' => 'required|is_string|trim|valid_email|is_unique[especialidades.Nombre_especialidad]'),
            array('field' => 'pass','label' => 'Contraseña','rules' => 'required|is_string|trim'),
              array('field' => 'pass2','label' => 'Confirmar Contraseña','rules' => 'required|is_string|trim|required|matches[pass]'),
            
        ),

        'login'
        => array(
            
           array('field' => 'correo','label' => 'Correo ','rules' => 'required|is_string|trim|valid_email'),
            array('field' => 'pass','label' => 'Contraseña','rules' => 'required|is_string|trim'),
             
            
        ),
        'addUsuarios'
        => array(
             array(
                'field' => 'nombre_de_usuario',
                'label' => 'Nombre',
                'rules' => 'required|is_string|trim|required'),            
            array(
                'field' => 'apellido',
                'label' => 'Apellido',
                'rules' => 'required|is_string|trim|required'),
            
           array(
            'field' => 'correo',
            'label' => 'Correo ',
            'rules' => 'required|is_string|trim|valid_email|is_unique[usuarios.correo]'),
            array(
                'field' => 'telefono',
                'label' => 'Telefono',
                'rules' => 'required|is_string|trim|required|max_length[10]|min_length[6]'),
            array(
                'field' => 'password',
                'label' => 'Contraseña',
                'rules' => 'required|is_string|trim|max_length[10]|min_length[6]'),
            array(
                'field' => 'password2',
                'label' => 'Confirmar Contraseña',
                'rules' => 'required|is_string|trim|required|matches[password]'),
            array(
                'field' => 'rol',
                'label' => 'Rol',
                'rules' => 'required|trim|integer'),

            
        ),  
         'editUsuarios'
        => array(
             array(
                'field' => 'nombre_de_usuario',
                'label' => 'Nombre',
                'rules' => 'required|is_string|trim|required'),   
                  array(
                'field' => 'telefono',
                'label' => 'Telefono',
                'rules' => 'required|is_string|trim|required|max_length[10]|min_length[6]'), 
          
            array(
                'field' => 'password',
                'label' => 'Contraseña',
                'rules' => 'required|is_string|trim'),
            array(
                'field' => 'password2',
                'label' => 'Confirmar Contraseña',
                'rules' => 'required|is_string|trim|required|matches[password]'),
            array(
                'field' => 'rol',
                'label' => 'Rol',
                'rules' => 'required|trim|integer'),

          


            
        ),  
          'editUsuarios2'
        => array(
             array(
                'field' => 'nombre_de_usuario',
                'label' => 'Nombre',
                'rules' => 'required|is_string|trim|required'),    

                  array(
            'field' => 'correo',
            'label' => 'Correo ',
            'rules' => 'required|is_string|trim|valid_email|is_unique[usuarios.correo]'),        
           
            
          
            array(
                'field' => 'password',
                'label' => 'Contraseña',
                'rules' => 'required|is_string|trim'),
            array(
                'field' => 'password2',
                'label' => 'Confirmar Contraseña',
                'rules' => 'required|is_string|trim|required|matches[password]'),
            array(
                'field' => 'rol',
                'label' => 'Rol',
                'rules' => 'required|trim|integer'),

        


            
        ),

         'addMembresia'
        => array(
             array(
                'field' => 'nombre',
                'label' => 'Nombre',
                'rules' => 'required|is_string|trim|required'),            
            array(
                'field' => 'apellido',
                'label' => 'Apellido',
                'rules' => 'required|is_string|trim|required'),
            
           array(
            'field' => 'cedula',
            'label' => 'Cedula ',
            'rules' => 'required|is_numeric|trim|is_unique[usuarios.cedula]|max_length[10]|min_length[6]'),
                      array(
            'field' => 'correo2',
            'label' => 'Email ',
            'rules' => 'required|is_string|trim|valid_email|is_unique[usuarios.correo]'),
            array(
                'field' => 'telefono',
                'label' => 'Telefono',
                'rules' => 'required|is_string|trim|required'),
            array(
                'field' => 'password',
                'label' => 'Contraseña',
                'rules' => 'required|is_string|trim|max_length[10]|min_length[6]|max_length[10]'),
            array(
                'field' => 'password2',
                'label' => 'Confirmar Contraseña',
                'rules' => 'required|is_string|trim|required|matches[password]'),
                      
        ),  
   //éste es el final    required|matches[password] 
);
